import React from "react";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { StyleSheet, Text } from "react-native";


import colors from "../styles/colors";

import { LoggedIn } from "../screens/LoggedIn";
import { Transfer } from "../screens/Transfer";
import { Pix } from "../screens/Pix";
import { DepositOne } from "../screens/DepositOne";
import { PayOne } from "../screens/PayOne";
import { RecentStatement } from "../screens/RecentStatement";
import { Charge } from "../screens/Charge";
import { Account } from "../screens/Account";
import { Lobby } from "../screens/Lobby";
import { BottomTabs } from "./BottomTabs";
 

import { DrawerContent } from "../screens/DrawerContent";
const Drawer = createDrawerNavigator();

export function DrawRoutes() {
    return (

        <Drawer.Navigator
            drawerContent={props => <DrawerContent {...props} />}
            initialRouteName={'Início'}
            drawerPosition='right'
            drawerType='front'
            drawerStyle={styles.drawer}
        >

            
            <Drawer.Screen name="Início" component={LoggedIn} /> 
           {/*  <Drawer.Screen name="Transferências" component={Transfer} />
            <Drawer.Screen name="Pix" component={Pix} />
            <Drawer.Screen name="Depósito" component={DepositOne} />
            <Drawer.Screen name="Pagar contas" component={PayOne} />
            <Drawer.Screen name="Extrato" component={RecentStatement} />
            <Drawer.Screen name="Cobrança" component={Charge} />
            <Drawer.Screen name="Conta" component={Account} />
            <Drawer.Screen name="Sair" component={Lobby} /> */}
            


        </Drawer.Navigator>

    )
}

const styles = StyleSheet.create({
    drawer: {
        backgroundColor: colors.dark_blue,
        width: '100%',
        marginTop: 120,
        zIndex: 1000,
        color: colors.white,
    }
})