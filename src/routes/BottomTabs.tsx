import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { Image, StyleSheet } from "react-native";



import { DrawRoutes } from "./DrawRoutes";

import { Notifications } from "../screens/Notications";
import { Manage } from "../screens/Manage";
import { Pix } from "../screens/Pix";
import { Account } from "../screens/Account";



import homeIcon from '../../assets/homeIcon.png';
import homeIconInactive from '../../assets/homeInactive.png';
import Bell from '../../assets/Bell.png';
import bellInactive from '../../assets/bellInactive.png';
import manage from '../../assets/manage.png';
import manageInactive from '../../assets/manageInactive.png';
import PixIcon from '../../assets/pixIcon.png';
import pixInactive from '../../assets/pixInactive.png';
import account from '../../assets/account.png';
import accountInactive from '../../assets/accountInactive.png';





import colors from "../styles/colors";
import { LoggedIn } from "../screens/LoggedIn";

const Tab = createBottomTabNavigator();

export function BottomTabs() {
    return (
        <Tab.Navigator

            initialRouteName="LoggedIn"
            tabBarOptions={{
                activeTintColor: colors.dark_blue,
                style: { paddingHorizontal: 10, backgroundColor: colors.white, height:85, paddingBottom:20 },
            }}

        >
            <Tab.Screen

                name="Início"
                component={DrawRoutes}
                options={{

                    tabBarIcon: ({ focused }) => (

                        <Image style={styles.Icon} source={focused ? homeIcon : homeIconInactive}></Image>

                    ),

                }}

            />
            <Tab.Screen
                name="Notificações" 
                component={Notifications}
                
                options={{
                    
                    tabBarIcon: ({ focused }) => (

                        <Image style={styles.Icon} source={focused ? Bell : bellInactive}></Image>

                    ),

                }}
            />
            <Tab.Screen
                name="Gestão"
                component={Manage}
                options={{

                    tabBarIcon: ({ focused }) => (

                        <Image style={styles.Icon} source={focused ? manage : manageInactive}></Image>

                    ),

                }}
            />
            <Tab.Screen
                name="Pix"
                component={Pix}
                options={{

                    tabBarIcon: ({ focused }) => (

                        <Image style={styles.Icon} source={focused ? PixIcon : pixInactive}></Image>

                    ),

                }}
            />
            <Tab.Screen
                name="Conta"
                component={Account}
                options={{

                    tabBarIcon: ({ focused }) => (

                        <Image style={styles.Icon} source={focused ? account : accountInactive}></Image>

                    ),

                }}
            />
        </Tab.Navigator>
    );
}


const styles = StyleSheet.create({
    Icon: {
        width: 25,
        height: 25,
        resizeMode: 'contain',
    }
})