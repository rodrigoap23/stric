import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import colors from "../styles/colors";

import { Lobby } from "../screens/Lobby";
import { CreateAccountOne } from "../screens/CreateAccountOne";
import { CreateAccountTwo } from "../screens/CreateAccountTwo";
import { CreateAccountThree } from "../screens/CreateAccountThree";
import { CreateAccountFour } from "../screens/CreateAccountFour";
import { CreateAccountFive } from "../screens/CreateAccountFive";
import { LoginAccountOne } from "../screens/LoginAccountOne";
import { LoginAccountTwo } from "../screens/LoginAccountTwo";

import { ForgotenAccountOne } from "../screens/ForgotenAccountOne";


import { DepositOne } from "../screens/DepositOne";
import { DepositTwo } from "../screens/DepositTwo";
import { DepositThree } from "../screens/DepositThree";
import { DepositFour } from "../screens/DepositFour";

import { Manage } from "../screens/Manage";

import { Pix } from "../screens/Pix";
import { PixQrScanner } from "../screens/PixQrScanner";
import { PixThree } from "../screens/PixThree";
import { PixFour } from "../screens/PixFour";
import { AuthPix } from "../screens/AuthPix";
import { PixSuccess } from "../screens/PixSuccess";
import { PixKeyTransfer} from "../screens/PixKeyTransfer";
import { PixKeyCpf} from "../screens/PixKeyCpf";
import { PixKeyHash} from "../screens/PixKeyHash";

import { PayOne } from "../screens/PayOne";
import { PayTwo } from "../screens/PayTwo";
import { PayThree } from "../screens/PayThree";
import { PaySuccess } from "../screens/PaySuccess";

import { Account } from "../screens/Account";
import { SignIn } from "../screens/SignIn";
import { Prices } from "../screens/Prices";


import { Notifications } from "../screens/Notications";

import { Transfer } from "../screens/Transfer";
import { TransferStricOne } from "../screens/TransferStricOne";
import { TransferStricTwo } from "../screens/TransferStricTwo";
import { TransferStricThree } from "../screens/TransferStricThree";
import { TransferStricAuth } from "../screens/TransferStricAuth";
import { TransferStricSuccess } from "../screens/TransferStricSuccess";

import { TransferTedOne } from "../screens/TransferTedOne";
import { TransfContact } from "../screens/TransfContact";
import { TransfTedTwo } from "../screens/TransfTedTwo";
import { TransfTedThree } from "../screens/TransfTedThree";
import { TransfTedAuth } from "../screens/TransfTedAuth";
import { TransfTedSuccess } from "../screens/TransfTedSuccess";

import { TransfRecents } from "../screens/TransfRecents";
import { TransfFavorites } from "../screens/TransfFavorites";
import { TransfTedNewContact } from "../screens/TransfTedNewContact";

import { Charge } from "../screens/Charge";
import { ChargePaySlip } from "../screens/ChargePaySlip";
import { ChargeAuth } from "../screens/ChargeAuth";
import { ChargeSuccess } from "../screens/ChargeSuccess";

import { ChargeLinkCard } from "../screens/ChargeLinkCard";
import { ChargeLinkCardAuth } from "../screens/ChargeLinkCardAuth";
import { ChargeLinkCardSuccess } from "../screens/ChargeLinkCardSuccess";

import { ChargePix } from "../screens/ChargePix";
import { ChargePixAuth } from "../screens/ChargePixAuth";
import { ChargePixSuccess } from "../screens/ChargePixSuccess";

import { ChargeOpenLink } from "../screens/ChargeOpenLink";
import { ChargeOpenLinkAuth } from "../screens/ChargeOpenLinkAuth";
import { ChargeOpenLinkSuccess } from "../screens/ChargeOpenLinkSuccess";

import { ChargeOpen } from "../screens/ChargeOpen";
import { ChargeReissuePaySlip } from "../screens/ChargeReissuePaySlip";
import { ChargeReissueLink } from "../screens/ChargeReissueLink";
import { ChargeReissuePix } from "../screens/ChargeReissuePix";
import { ChargeReissueOpenLink } from "../screens/ChargeReissueOpenLink";

import { RecentStatement } from "../screens/RecentStatement";
import { SearchStatement } from "../screens/SearchStatement";
import { SearchCharge } from "../screens/SearchCharge";    

import { MyAccount } from "../screens/MyAccount";    
import { PasswordChange } from "../screens/PasswordChange";    
import { PasswordMyAccountChange } from "../screens/PasswordMyAccountChange";    
import { Users } from "../screens/Users";    
import { AddUser } from "../screens/AddUser";    
import { EditUser } from "../screens/EditUser";    








import { BottomTabs } from "./BottomTabs";
import { AccountBottomTabs } from "./AccountBottomTabs";
import { DrawRoutes } from "./DrawRoutes";
import { LoggedIn } from "../screens/LoggedIn";

const stackRoutes = createStackNavigator();

const AppRoutes: React.FC = () => (
    <stackRoutes.Navigator
        headerMode="none"
        screenOptions={{
            cardStyle: {
                backgroundColor: colors.white,
            }
        }}
    >
        <stackRoutes.Screen
            name="Lobby"
            component={Lobby}
        />
        <stackRoutes.Screen
            name="CreateAccountOne"
            component={CreateAccountOne}
        />
         <stackRoutes.Screen
            name="CreateAccountTwo"
            component={CreateAccountTwo}    
        />
         <stackRoutes.Screen
            name="CreateAccountThree"
            component={CreateAccountThree}    
        />
         <stackRoutes.Screen
            name="CreateAccountFour"
            component={CreateAccountFour}    
        />
         <stackRoutes.Screen
            name="CreateAccountFive"
            component={CreateAccountFive}    
        />
         <stackRoutes.Screen
            name="LoginAccountOne"
            component={LoginAccountOne}    
        />
         <stackRoutes.Screen
            name="LoginAccountTwo"
            component={LoginAccountTwo}    
        />
         <stackRoutes.Screen
            name="ForgotenAccountOne"
            component={ForgotenAccountOne}    
        />
        <stackRoutes.Screen
            name="LoggedIn"
            component={BottomTabs}
        />
         <stackRoutes.Screen
            name="DepositOne"
            component={DepositOne}
        />
         <stackRoutes.Screen
            name="DepositTwo"
            component={DepositTwo}
        />
         <stackRoutes.Screen
            name="DepositThree"
            component={DepositThree}
        />
         <stackRoutes.Screen
            name="DepositFour"
            component={DepositFour}
        />
         <stackRoutes.Screen
            name="Transfer"
            component={Transfer}
        />
         <stackRoutes.Screen
            name="Notifications"
            component={Notifications}
        />
         <stackRoutes.Screen
            name="Manage"
            component={Manage}
        />
         <stackRoutes.Screen
            name="Pix"
            component={Pix}
        />
         <stackRoutes.Screen
            name="PixQrScanner"
            component={PixQrScanner}
        />
         <stackRoutes.Screen
            name="PixThree"
            component={PixThree}
        />
         <stackRoutes.Screen
            name="PixFour"
            component={PixFour}
        />
         <stackRoutes.Screen
            name="PixKeyTransfer"
            component={PixKeyTransfer}
        />
         <stackRoutes.Screen
            name="PixKeyCpf"
            component={PixKeyCpf}
        />
         <stackRoutes.Screen
            name="PixKeyHash"
            component={PixKeyHash}
        />
         <stackRoutes.Screen
            name="AuthPix"
            component={AuthPix}
        />
         <stackRoutes.Screen
            name="PixSuccess"
            component={PixSuccess}
        />
         <stackRoutes.Screen
            name="PayOne"
            component={PayOne}
        />
         <stackRoutes.Screen
            name="PayTwo"
            component={PayTwo}
        />
         <stackRoutes.Screen
            name="PayThree"
            component={PayThree}
        />
         <stackRoutes.Screen
            name="PaySuccess"
            component={PaySuccess}
        />

        

         <stackRoutes.Screen
            name="Account"
            component={Account}
        />
         



         <stackRoutes.Screen
            name="SignIn"
            component={SignIn}
        />
         <stackRoutes.Screen
            name="Prices"
            component={Prices}
        />

         <stackRoutes.Screen
            name="TransferStricOne"
            component={TransferStricOne}
        />
         <stackRoutes.Screen
            name="TransferStricTwo"
            component={TransferStricTwo}
        />
         <stackRoutes.Screen
            name="TransferStricThree"
            component={TransferStricThree}
        />
         <stackRoutes.Screen
            name="TransferStricAuth"
            component={TransferStricAuth}
        />
         <stackRoutes.Screen
            name="TransferStricSuccess"
            component={TransferStricSuccess}
        />
         <stackRoutes.Screen
            name="TransferTedOne"
            component={TransferTedOne}
        />
         <stackRoutes.Screen
            name="TransfContact"
            component={TransfContact}
        />
         <stackRoutes.Screen
            name="TransfTedTwo" 
            component={TransfTedTwo}
        />
         <stackRoutes.Screen
            name="TransfTedThree" 
            component={TransfTedThree}
        />
         <stackRoutes.Screen
            name="TransfTedAuth" 
            component={TransfTedAuth}
        />
         <stackRoutes.Screen
            name="TransfTedSuccess" 
            component={TransfTedSuccess}
        />
         <stackRoutes.Screen
            name="TransfRecents" 
            component={TransfRecents}
        />
         <stackRoutes.Screen
            name="TransfFavorites" 
            component={TransfFavorites}
        />
         <stackRoutes.Screen
            name="TransfTedNewContact" 
            component={TransfTedNewContact}
        />
         <stackRoutes.Screen
            name="Charge" 
            component={Charge}
        />
         <stackRoutes.Screen
            name="ChargePaySlip" 
            component={ChargePaySlip}
        />
         <stackRoutes.Screen
            name="ChargeAuth" 
            component={ChargeAuth}
        />
         <stackRoutes.Screen
            name="ChargeSuccess" 
            component={ChargeSuccess}
        />
         <stackRoutes.Screen
            name="ChargeLinkCard" 
            component={ChargeLinkCard}
        />
         <stackRoutes.Screen
            name="ChargeLinkCardAuth" 
            component={ChargeLinkCardAuth}
        />
         <stackRoutes.Screen
            name="ChargeLinkCardSuccess" 
            component={ChargeLinkCardSuccess}
        />
         <stackRoutes.Screen
            name="ChargePix" 
            component={ChargePix}
        />
         <stackRoutes.Screen
            name="ChargePixAuth" 
            component={ChargePixAuth}
        />
         <stackRoutes.Screen
            name="ChargePixSuccess" 
            component={ChargePixSuccess}
        />
         <stackRoutes.Screen
            name="ChargeOpenLink" 
            component={ChargeOpenLink}
        />
         <stackRoutes.Screen
            name="ChargeOpenLinkAuth" 
            component={ChargeOpenLinkAuth}
        />
         <stackRoutes.Screen
            name="ChargeOpenLinkSuccess" 
            component={ChargeOpenLinkSuccess}
        />
         <stackRoutes.Screen
            name="ChargeOpen" 
            component={ChargeOpen}
        />
         <stackRoutes.Screen
            name="ChargeReissuePaySlip" 
            component={ChargeReissuePaySlip}
        />
         <stackRoutes.Screen
            name="ChargeReissueLink" 
            component={ChargeReissueLink}
        />
         <stackRoutes.Screen
            name="ChargeReissuePix" 
            component={ChargeReissuePix}
        />
         <stackRoutes.Screen
            name="ChargeReissueOpenLink" 
            component={ChargeReissueOpenLink}
        />
         <stackRoutes.Screen
            name="RecentStatement" 
            component={RecentStatement}
        />
         <stackRoutes.Screen
            name="SearchStatement" 
            component={SearchStatement}
        />
         <stackRoutes.Screen
            name="SearchCharge" 
            component={SearchCharge}
        />
         <stackRoutes.Screen
            name="MyAccount" 
            component={MyAccount}
        />
         <stackRoutes.Screen
            name="PasswordChange" 
            component={PasswordChange}
        />
         <stackRoutes.Screen
            name="PasswordMyAccountChange" 
            component={PasswordMyAccountChange}
        />
         <stackRoutes.Screen
            name="Users" 
            component={Users}
        />
         <stackRoutes.Screen
            name="AddUser" 
            component={AddUser}
        />
         <stackRoutes.Screen
            name="EditUser" 
            component={EditUser}
        />
        
         
      
    </stackRoutes.Navigator>
)

export default AppRoutes;