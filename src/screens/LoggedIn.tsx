import { Ionicons } from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/core";
import { DrawerActions } from "@react-navigation/native";
import React, { useState } from "react";
import {
  Image,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Dimensions
} from "react-native";
import AB from "../../assets/AB.png";
import chargeIcon from "../../assets/chargeIcon.png";
import eye from "../../assets/eye.png";
import eyeoff from "../../assets/eyeOff.png";
import food from "../../assets/food.png";
import hamburger from "../../assets/hamburger.png";
import logoWhite from "../../assets/logoWhite.png";
import transferIcon from "../../assets/money-transfer.png";
import music from "../../assets/music.png";
import payIcon from "../../assets/payIcon.png";
import walltetIcon from "../../assets/walletIcon.png";
import colors from "../styles/colors";
import fonts from "../styles/fonts";

export function LoggedIn() {

  const [hideValue, setHideValue] = useState(true);


  const navigation = useNavigation();

  function handleDeposit() {
    navigation.navigate("DepositOne");
  }
  function handleTransfer() {
    navigation.navigate("Transfer");
  }
  function handleClose() {
    navigation.navigate("Lobby");
  }
  function handleNext() {
    navigation.navigate("LoginAccountTwo");
  }
  function handleCreateAccount() {
    navigation.navigate("CreateAccountOne");
  }

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar />
      <View style={styles.header}>
        <View style={styles.headerRow}>
          <Image style={styles.logo} source={logoWhite} />
          <View style={styles.userOptions}>
            <Text style={styles.userName}>Martin Ferramentas</Text>
            <Ionicons style={styles.iconsUserOptions} name="chevron-down" />
          </View>
        </View>
        <View style={styles.headerMain}>
          <View style={styles.wrapBalance}>
            <Text style={styles.balanceTitle}>Saldo</Text>

           
            {hideValue ? (
            <View style={styles.balance}>
              <View style={styles.balanceRow}>
                <Text style={styles.balanceValue}>-</Text>
               
              </View>
              <TouchableOpacity onPress={()=>{setHideValue(false)}}>
                <Image style={styles.eyeIcon} source={eye} />
              </TouchableOpacity>
            </View>
            ):(
              <View style={styles.balance}>
              <View style={styles.balanceRow}>
              
                <Text style={styles.balanceValue}>R$ 24.895,35</Text>
              </View>
              <TouchableOpacity onPress={()=>{setHideValue(true)}}>
                <Image style={styles.eyeIcon} source={eyeoff} />
              </TouchableOpacity>
            </View>
            )}
          </View>
          <TouchableOpacity
            onPress={() => navigation.dispatch(DrawerActions.openDrawer())}
            style={styles.menuButton}
          >
            <Image style={styles.menuIcon} source={hamburger} />
          </TouchableOpacity>
        </View>
      </View>
      <View style={styles.content}>
        <View style={styles.wrapNavBox}>
          <TouchableOpacity onPress={handleDeposit} style={styles.box1}>
            <View style={styles.boxTile}></View>
            <Image style={styles.boxIcon} source={walltetIcon} />
            <Text style={styles.boxTitle}>Depositar</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={handleTransfer} style={styles.box1}>
            <View style={styles.boxTile}></View>
            <Image style={styles.boxIcon} source={transferIcon} />
            <Text style={styles.boxTitle}>Transferir</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate("PayOne");
            }}
            style={styles.box1}
          >
            <View style={styles.boxTile}></View>
            <Image style={styles.boxIcon} source={payIcon} />
            <Text style={styles.boxTitle}>Pagar</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={()=>{navigation.navigate("Charge")}} style={styles.box1}>
            <View style={styles.boxTile}></View>
            <Image style={styles.boxIcon} source={chargeIcon} />
            <Text style={styles.boxTitle}>Cobrar</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.linkWrap}>
          <TouchableOpacity>
            <Text style={styles.linkTitle}>Extrato Completo</Text>
          </TouchableOpacity>
          <Ionicons style={styles.linkIcon} name="chevron-forward" />
        </View>
        <ScrollView>
          <View style={styles.WrapExtract}>
            <View style={styles.extracstInner}>
              <View style={styles.wrapDate}>
                <View style={styles.dateTile}>
                  <Text style={styles.date}>13 ABR.</Text>
                </View>
              </View>

              <View style={styles.wrapPurchases}>
                <View style={styles.purchase}>
                  <View style={styles.purchaseTypeBox}>
                    <Image style={styles.purchaseTypeIcon} source={food} />
                  </View>
                  <View style={styles.purchaseInner}>
                    <Text style={styles.purchaseTitleType}>Débito</Text>
                    <Text style={styles.purchaseTitle}>Starbuck</Text>
                  </View>
                  <View style={styles.purchaseValue}>
                    <Text style={styles.value}>-R$32,30</Text>
                  </View>
                  <View style={styles.purchaseBoxIcon}>
                    <Ionicons
                      style={styles.purchaseChevron}
                      name="chevron-forward"
                    />
                  </View>
                </View>

                <View style={styles.purchase}>
                  <View style={styles.purchaseTypeBox}>
                    <Image style={styles.purchaseTypeIcon} source={music} />
                  </View>
                  <View style={styles.purchaseInner}>
                    <Text style={styles.purchaseTitleType}>Débito</Text>
                    <Text style={styles.purchaseTitle}>Spotify</Text>
                  </View>
                  <View style={styles.purchaseValue}>
                    <Text style={styles.value}>-R$7,90</Text>
                  </View>
                  <View style={styles.purchaseBoxIcon}>
                    <Ionicons
                      style={styles.purchaseChevron}
                      name="chevron-forward"
                    />
                  </View>
                </View>

                <View style={styles.purchase}>
                  <View style={styles.purchaseTypeBox}>
                    <Image style={styles.purchaseTypeIcon} source={AB} />
                  </View>
                  <View style={styles.purchaseInner}>
                    <Text style={styles.purchaseTitleType}>
                      Transferência Recebida
                    </Text>
                    <Text style={styles.purchaseTitle}>Adalberto Brito</Text>
                  </View>
                  <View style={styles.purchaseValue}>
                    <Text style={styles.valuePositive}>+R$12.360,00</Text>
                  </View>
                  <View style={styles.purchaseBoxIcon}>
                    <Ionicons
                      style={styles.purchaseChevron}
                      name="chevron-forward"
                    />
                  </View>
                </View>
              </View>
            </View>

            <View style={styles.extracstInner}>
              <View style={styles.wrapDate}>
                <View style={styles.dateTile}>
                  <Text style={styles.date}>12 ABR.</Text>
                </View>
              </View>

              <View style={styles.wrapPurchases}>
                <View style={styles.purchase}>
                  <View style={styles.purchaseTypeBox}>
                    <Image style={styles.purchaseTypeIcon} source={food} />
                  </View>
                  <View style={styles.purchaseInner}>
                    <Text style={styles.purchaseTitleType}>
                      Pagamento de Contas
                    </Text>
                    <Text style={styles.purchaseTitle}>Conta de Luz</Text>
                  </View>
                  <View style={styles.purchaseValue}>
                    <Text style={styles.value}>-R$111,23</Text>
                  </View>
                  <View style={styles.purchaseBoxIcon}>
                    <Ionicons
                      style={styles.purchaseChevron}
                      name="chevron-forward"
                    />
                  </View>
                </View>

                <View style={styles.purchase}>
                  <View style={styles.purchaseTypeBox}>
                    <Image style={styles.purchaseTypeIcon} source={music} />
                  </View>
                  <View style={styles.purchaseInner}>
                    <Text style={styles.purchaseTitleType}>
                      Pagamento de Contas
                    </Text>
                    <Text style={styles.purchaseTitle}>Condimínio</Text>
                  </View>
                  <View style={styles.purchaseValue}>
                    <Text style={styles.value}>-R$442,35</Text>
                  </View>
                  <View style={styles.purchaseBoxIcon}>
                    <Ionicons
                      style={styles.purchaseChevron}
                      name="chevron-forward"
                    />
                  </View>
                </View>
              </View>
            </View>
          </View>
        </ScrollView>
      </View>

      {/* <View style={styles.nav}>
               <TouchableOpacity style={styles.navButton}>
                    <Image style={styles.navIcon} source={homeIcon}/>
                    <Text style={styles.navTitle}>
                        Início
                    </Text>
               </TouchableOpacity>
               <TouchableOpacity style={styles.navButton}>
                    <Image style={styles.navIcon} source={tickInactive}/>
                    <Text style={styles.navTitle}>
                        Aprovações
                    </Text>
               </TouchableOpacity>
               <TouchableOpacity style={styles.navButton}>
                    <Image style={styles.navIcon} source={manageInactive}/>
                    <Text style={styles.navTitle}>
                        Gestão
                    </Text>
               </TouchableOpacity>
               <TouchableOpacity style={styles.navButton}>
                    <Image style={styles.navIcon} source={pixInactive}/>
                    <Text style={styles.navTitle}>
                        Pix
                    </Text>
               </TouchableOpacity>
               <TouchableOpacity style={styles.navButton}>
                    <Image style={styles.navIcon} source={accountInactive}/>
                    <Text style={styles.navTitle}>
                        Conta
                    </Text>
               </TouchableOpacity>
               
               
            </View> */}
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    alignItems: "center",
    backgroundColor: colors.dark_blue,
  },
  header: {
    width: "100%",
    height: 228,
  },
  headerRow: {
    flexDirection: "row",
    marginTop: 60,
    paddingHorizontal: 28,
    justifyContent: "space-between",
    alignItems: "center",
  },
  logo: {
    width: 61,
    height: 23,
  },
  userOptions: {
    flexDirection: "row",
    justifyContent: "center",
    borderRadius: 20,
    paddingHorizontal: 14,
    paddingVertical: 10,
    alignItems: "center",
    backgroundColor: colors.white,
  },
  userName: {
    color: colors.dark_blue,
    fontFamily: fonts.regular,
    marginRight: 8,
  },
  iconsUserOptions: {
    color: colors.light_blue,
    fontSize: 20,
  },
  headerMain: {
    flexDirection: "row",
    marginTop: 18,
    justifyContent: "space-between",
    paddingHorizontal: 28,
  },
  wrapBalance: {
    flex:1
  },

  balanceTitle: {
    fontFamily: fonts.bold,
    color: colors.light_blue,
    fontSize: 14,
  },
  balance: {
    
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-start",
    
  },
  balanceRow: {
   width: "50%",
  
  },
  balanceValue: {

    fontFamily: fonts.bold,
    color: colors.white,
    fontSize: 28,
  },
  eyeIcon: {
    marginTop: 5,
    width: 24,
    height: 18,
    marginLeft: 20,
    resizeMode: "contain",
  },
  menuButton: {
    marginTop: 20,
    alignItems: "center",
    justifyContent: "center",
  },
  menuIcon: {
    width: 23,
    height: 18,
  },

  content: {
    flex: 1,
    width: "100%",
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: colors.white,
  },
  wrapNavBox: {
    flexDirection: "row",
    justifyContent: "space-around",
    marginTop: -25,
    paddingHorizontal: 20,
  },
  box1: {
     width:Dimensions.get('screen').width/5,
     height:Dimensions.get('screen').width/4,
    //width: 85,
    //height: 106,
    paddingBottom: 10,
   // borderWidth: 1,
    borderRadius: 12,
    borderColor: "#e3e3e3",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: colors.white,

     // borderWidth: 0,
    // borderColor: colors.light_gray,
   // backgroundColor: "#fff",
    shadowColor: "#a9a9a9",
    borderBottomWidth: 0,
    shadowOffset: { width: 5, height: 9 },
    shadowOpacity: 0.1,
    shadowRadius: 2,
    elevation: 10,


  },
  boxTile: {
    width: 40,
    height: 4,
    backgroundColor: colors.light_blue,
  },
  boxIcon: {
    width: 27,
    height: 23,
    resizeMode: "contain",
  },
  boxTitle: {
    fontFamily: fonts.medium,
    color: colors.dark_blue,
    fontSize: 15,
  },
  linkWrap: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    paddingVertical: 26,
  },
  linkTitle: {
    fontFamily: fonts.medium,
    color: colors.dark_blue,
    fontSize: 14,
  },
  linkIcon: {
    color: colors.light_blue,
    fontSize: 16,
    marginTop: 2,
  },
  WrapExtract: {
    marginTop: 20,
    justifyContent: "center",
    alignItems: "center",
  },
  extracstInner: {
    width: "100%",
    borderTopWidth: 1,
    borderTopColor: colors.light_gray,
  },
  wrapDate: {
    justifyContent: "center",
    alignItems: "center",
  },
  dateTile: {
    width: 70,
    height: 27,
    marginTop: -15,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 18,
    backgroundColor: colors.white,
    borderWidth: 1,
    borderColor: colors.light_gray,
  },
  date: {
    fontFamily: fonts.regular,
    color: colors.dark_blue,
    fontSize: 12,
  },
  purchase: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: 28,
    paddingVertical: 10,
  },
  purchaseTypeBox: {
    borderWidth: 1,
    borderColor: colors.light_gray,
    borderRadius: 10,
    backgroundColor: colors.white,
    justifyContent: "center",
    alignItems: "center",
    width: 45,
    height: 45,
  },
  purchaseTypeIcon: {
    width: 18.5,
    height: 16,
    resizeMode: "contain",
  },
  purchaseTitleType: {
    fontFamily: fonts.regular,
    color: colors.gray,
  },
  purchaseInner: {
    textAlign: "left",
    flex: 1,
    paddingLeft: 15,
  },
  purchaseTitle: {
    fontFamily: fonts.regular,
    fontSize: 17,
    color: colors.dark_blue,
  },
  purchaseValue: {
    alignItems: "center",
    justifyContent: "center",
  },
  wrapPurchases: {
    paddingBottom: 20,
  },
  value: {
    fontFamily: fonts.regular,
    fontSize: 17,
    color: colors.red,
  },
  valuePositive: {
    fontFamily: fonts.regular,
    fontSize: 17,
    color: colors.green,
  },
  purchaseBoxIcon: {
    alignItems: "center",
    justifyContent: "center",
  },
  purchaseChevron: {
    color: colors.gray,
    fontSize: 16,
  },
  nav: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: 28,
    height: 83,
    backgroundColor: colors.white,
  },
  navIcon: {
    width: 26,
    height: 26,
    justifyContent: "space-between",
  },
  navButton: {
    justifyContent: "center",
    alignItems: "center",
  },
  navTitle: {
    fontFamily: fonts.medium,
  },
});
