import { useNavigation } from "@react-navigation/core";
import React from "react";
import {
  Image,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  TextInput,
  View,
} from "react-native";
import logoWhite from "../../assets/logoWhite.png";
import ConfirmButton from "../components/ConfirmButton";
import HeaderSection from "../components/HeaderSection";
import colors from "../styles/colors";
import fonts from "../styles/fonts";

export function CreateAccountThree() {
  const navigation = useNavigation();

  function handleClose() {
    navigation.navigate("Lobby");
  }
  function handleBack() {
    navigation.navigate("CreateAccountTwo");
  }
  function handleNext() {
    navigation.navigate("CreateAccountFour");
  }
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar />
      <Image source={logoWhite} style={styles.imageLogo} />
      <View style={styles.content}>
        <HeaderSection
          title={"Nome Fantasia"}
          rightIcon={true}
          rightNavigation={"Lobby"}
          leftIcon={true}
          leftNavigation={"CreateAccountTwo"}
        />

        <TextInput keyboardType="default" style={styles.inputForm} />
        <View style={styles.Footer}>
          <ConfirmButton title={"Confirmar"} navTo={"CreateAccountFour"} />
        </View>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    alignItems: "center",
    backgroundColor: colors.dark_blue,
  },
  imageLogo: {
    width: 62,
    height: 23,
    marginTop: 38,
    marginBottom: 38,
  },
  content: {
    flex: 1,
    width: "100%",
    justifyContent: "flex-start",
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: colors.white,
    paddingTop: 40,
  },
  form: {
    justifyContent: "center",
    alignItems: "center",
  },
  inputForm: {
    width: "100%",
    color: colors.light_blue,
    fontSize: 30,
    marginTop: 32,
    marginHorizontal: 29,
    fontFamily: fonts.regular,
    borderLeftWidth: 1,
    borderLeftColor: colors.light_gray,
  },
  Footer: {
    backgroundColor: colors.white,
    flex: 1,
    justifyContent: "flex-end",
    width: "100%",
  },
});
