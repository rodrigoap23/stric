import React, { useState } from "react";
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  View,
} from "react-native";
import { KeycodeInput } from "react-native-keycode";
import ConfirmButton from "../components/ConfirmButton";
import HeaderSection from "../components/HeaderSection";
import colors from "../styles/colors";
import fonts from "../styles/fonts";

export function TransfTedAuth() {
  const [money, setMoney] = useState("1400,00");
  const [isEnabled, setIsEnabled] = useState(true);
  const toggleSwitch = () => setIsEnabled((previousState) => !previousState);

  return (
    <>
      <ScrollView>
        <SafeAreaView style={styles.container}>
          <StatusBar />
          <View style={styles.header}>
            <Text style={styles.userTitle}>Martin Ferramentas</Text>
          </View>
          <View style={styles.content}>
            <HeaderSection
              title="Autenticação"
              leftIcon={true}
              leftNavigation={"TransfTedThree"}
              rightIcon={false}
              rightNavigation={"LoggedIn"}
            />
            <View style={styles.wrapInner}>
              <KeycodeInput
                alphaNumeric={false}
                numeric={true}
                onComplete={() => {}}
              />
            </View>
          </View>
        </SafeAreaView>
      </ScrollView>

      <ConfirmButton title="Confirmar" navTo={"TransfTedSuccess"} />
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    alignItems: "center",
    backgroundColor: colors.dark_blue,
  },
  header: {
    paddingVertical: 34,
  },
  userTitle: {
    fontFamily: fonts.bold,
    color: colors.white,
    fontSize: 18,
    textAlign: "center",
  },
  content: {
    flex: 1,
    width: "100%",
    justifyContent: "flex-start",
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: colors.white,
    paddingTop: 40,
  },
  wrapInner: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    width: "100%",
    height: "100%",
    paddingHorizontal: 29,
    paddingTop: 136,
  },
  char: {
    fontSize: 40,
  },
});
