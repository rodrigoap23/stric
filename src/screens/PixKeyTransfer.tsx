import { Ionicons } from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/core";
import React from "react";
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import BoxBalance from "../components/BoxBalance";
import ConfirmButton from "../components/ConfirmButton";
import HeaderSection from "../components/HeaderSection";
import colors from "../styles/colors";
import fonts from "../styles/fonts";

export function PixKeyTransfer() {
  const navigation = useNavigation();
  return (
    <>
      <ScrollView>
        <SafeAreaView style={styles.container}>
          <StatusBar />
          <View style={styles.header}>
            <Text style={styles.userTitle}>Martin Ferramentas</Text>
          </View>
          <View style={styles.content}>
            <HeaderSection
              title="Pix com chave"
              leftIcon={true}
              leftNavigation={"Pix"}
              rightIcon={false}
              rightNavigation={"LoggedIn"}
            />

            <View style={styles.wrapInner}>
              <BoxBalance balance={"24.895,36"} />
              <Text style={styles.sectionTitle}>Escolha uma chave</Text>
            </View>
            <View style={styles.wrapFooter}>
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate("PixKeyCpf");
                }}
              >
                <View style={styles.sectionFooter}>
                  <Text style={styles.historyButton}>CPF/CNPJ</Text>
                  <Ionicons name="chevron-forward" style={styles.arrow} />
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View
                  style={[
                    styles.sectionFooter,
                    { borderTopWidth: 1, borderTopColor: colors.light_gray },
                  ]}
                >
                  <Text style={styles.historyButton}>Celular</Text>
                  <Ionicons name="chevron-forward" style={styles.arrow} />
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View
                  style={[
                    styles.sectionFooter,
                    { borderTopWidth: 1, borderTopColor: colors.light_gray },
                  ]}
                >
                  <Text style={styles.historyButton}>E-mail</Text>
                  <View style={styles.wrapNotify}></View>
                  <Ionicons name="chevron-forward" style={styles.arrow} />
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View
                  style={[
                    styles.sectionFooter,
                    { borderTopWidth: 1, borderTopColor: colors.light_gray },
                  ]}
                >
                  <Text style={styles.historyButton}>Pagamentos</Text>
                  <View style={styles.wrapNotify}></View>
                  <Ionicons name="chevron-forward" style={styles.arrow} />
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </SafeAreaView>
      </ScrollView>

      <ConfirmButton title="Continuar" navTo={"AuthPix"} />
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    alignItems: "center",
    backgroundColor: colors.dark_blue,
  },
  header: {
    paddingVertical: 34,
  },
  userTitle: {
    fontFamily: fonts.bold,
    color: colors.white,
    fontSize: 18,
    textAlign: "center",
  },
  content: {
    flex: 1,
    width: "100%",
    justifyContent: "flex-start",
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: colors.white,
    paddingTop: 40,
  },
  wrapInner: {
    width: "100%",
    paddingHorizontal: 29,
    paddingTop: 20,
  },

  wrapFooter: {
    justifyContent: "flex-end",
    paddingHorizontal: 29,
  },
  sectionFooter: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingVertical: 28,
    alignItems: "center",
  },
  sectionTitle: {
    fontFamily: fonts.regular,
    color: colors.light_blue,
    fontSize: 16,
    marginTop: 36,
  },
  historyButton: {
    fontFamily: fonts.regular,
    color: colors.dark_blue,
    fontSize: 16,
  },
  arrow: {
    fontSize: 24,
    color: colors.light_blue,
  },
  wrapNotify: {
    flex: 1,
    justifyContent: "flex-end",
    alignItems: "flex-end",
    paddingRight: 10,
  },
  iconStar: {
    fontSize: 24,
    color: colors.orange,
    justifyContent: "flex-end",
  },
  badgeNotification: {
    backgroundColor: colors.light_blue,
    paddingHorizontal: 7,
    borderRadius: 5,
  },
  badgeInner: {
    fontFamily: fonts.bold,
    color: colors.white,
  },
});
