import { useNavigation } from "@react-navigation/core";
import React, { useState } from "react";
import {
  Animated,
  Image,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  View,
} from "react-native";
import eye from "../../assets/eye.png";
import eyeOff from "../../assets/eyeOff.png";
import logoWhite from "../../assets/logoWhite.png";
import ConfirmButton from "../components/ConfirmButton";
import HeaderSection from "../components/HeaderSection";
import colors from "../styles/colors";

export function CreateAccountFour() {
  const [input, setInput] = useState("");

  const [hidePass, setHidePass] = useState(true);

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar />
      <Image source={logoWhite} style={styles.imageLogo} />
      <Animated.View style={styles.content}>
        <HeaderSection
          title={"Crie uma senha"}
          leftIcon={true}
          leftNavigation={"CreateAccountThree"}
          rightIcon={true}
          rightNavigation={"Lobby"}
        />

        <View style={styles.inputWrap}>
          <TextInput
            style={styles.inputForm}
            keyboardType="default"
            value={input}
            onChangeText={(text) => setInput(text)}
            secureTextEntry={hidePass}
          />
          <TouchableOpacity onPress={() => setHidePass(!hidePass)}>
            {hidePass ? (
              <Image source={eyeOff} style={styles.eyeOff} />
            ) : (
              <Image source={eye} style={styles.eye} />
            )}
          </TouchableOpacity>
        </View>
        <View style={styles.Footer}>
          <ConfirmButton title={"Continuar"} navTo={"CreateAccountFive"} />
        </View>
      </Animated.View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    alignItems: "center",
    backgroundColor: colors.dark_blue,
  },
  imageLogo: {
    width: 62,
    height: 23,
    marginTop: 38,
    marginBottom: 38,
  },
  content: {
    flex: 1,
    width: "100%",
    justifyContent: "flex-start",
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: colors.white,
    paddingTop: 40,
  },
  form: {
    justifyContent: "center",
    alignItems: "center",
  },
  inputForm: {
    width: "85%",
    color: colors.light_blue,
    fontSize: 30,
    //marginTop: 32,
    fontFamily: "Flexo-Regular",
    borderLeftWidth: 1,
    borderLeftColor: colors.light_gray,
    marginRight: 15,
    // borderWidth: 1,
  },
  inputWrap: {
    paddingVertical: 32,
    paddingHorizontal: 25,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  eyeOff: {
    width: 24,
    height: 19,
  },
  eye: {
    width: 25,
    height: 19,
  },
  Footer: {
    backgroundColor: colors.white,
    flex: 1,
    justifyContent: "flex-end",
    width: "100%",
  },
});
