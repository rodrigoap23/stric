import { useNavigation } from "@react-navigation/core";
import React from "react";
import {
    SafeAreaView,
    StatusBar,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from "react-native";
import about from "../../assets/about.png";
import barcode4 from "../../assets/barcode4.png";
import qrcode from "../../assets/qr-codecharge.png";
import cardcharge from "../../assets/cardcharge2.png";
import link from "../../assets/link2.png";

import barcodecharge from '../../assets/barcode-charge.png';
import qrcodered from '../../assets/qrcode-charge-red.png';
import cardchargeblue from '../../assets/cardcharge.png';
import linkblue from '../../assets/link.png';   


import HeaderSection from "../components/HeaderSection";
import ListItem from "../components/ListItem";
import colors from "../styles/colors";
import fonts from "../styles/fonts";
import { TextInput } from "react-native-gesture-handler";
import { Picker } from "@react-native-community/picker";

export function SearchCharge() {
    const navigation = useNavigation();

    function handleLnk(lnk: string) { }
    return (
        <SafeAreaView style={styles.container}>
            <StatusBar />
            <View style={styles.header}>
                <Text style={styles.userTitle}>Martin Ferramentas</Text>
            </View>
            <View style={styles.content}>
                <HeaderSection 
                    leftIcon={true}
                    leftNavigation={"Charge"}
                    rightIcon={false}
                    rightNavigation={"LoggedIn"}    
                    title={"Buscar cobrança"}
                    

                />
                <View style={styles.wrapForm}>
                    <TextInput style={styles.input} placeholder="Buscar"/>
                    <Picker style={styles.input}>
                        <Picker.Item label="Escolher status" value="0" />
                    </Picker>
                </View>
                <View style={styles.wrapNotifications}>

                    <TouchableOpacity
                        onPress={() => {
                            navigation.navigate("ChargeReissuePaySlip");
                        }}
                    >
                        <ListItem
                            icon={barcode4}
                            title={"Vecimento em 12/05/2021"}
                            name={"Boleto | Em aberto"}
                            values={"R$ 3.000,00"}
                            color={colors.orange}
                            colorValue={colors.orange}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => { navigation.navigate("ChargeReissuePix") }}>
                        <ListItem
                            icon={qrcode}
                            title={"Vencimento em 07/08/2021"}
                            name={"Pix | Em aberto"}
                            values={"R$ 420,00"}
                            color={colors.orange}
                            colorValue={colors.orange}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => { navigation.navigate("ChargeReissueLink") }}>
                        <ListItem
                            icon={cardcharge}
                            title={"Vencimento em 31/05/2021"}
                            name={"Link Cartão | Em aberto"}
                            values={"R$ 2.000,00"}
                            color={colors.orange}
                            colorValue={colors.orange}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => { navigation.navigate("ChargeReissueOpenLink") }}>
                    <ListItem
                        icon={link}
                        title={"Vencimento em 31/05/2021"}
                        name={"Link | Em aberto"}
                        values={"R$ 6.000,00"}
                        color={colors.orange}
                        colorValue={colors.orange}
                    />
                    </TouchableOpacity>

                </View>
                
            </View>
            
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: "100%",
        alignItems: "center",
        backgroundColor: colors.dark_blue,
    },
    header: {
        paddingVertical: 34,
    },
    userTitle: {
        fontFamily: fonts.bold,
        color: colors.white,
        fontSize: 18,
        textAlign: "center",
    },
    content: {
        flex: 1,
        width: "100%",
        justifyContent: "flex-start",
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15,
        backgroundColor: colors.white,
        paddingTop: 40,
    },
    wrapNotifications: {
        paddingBottom: 20,
        paddingHorizontal: 29,
        paddingTop: 30,
    },
    value: {
        fontFamily: fonts.regular,
        fontSize: 17,
    },
    valuePositive: {
        fontFamily: fonts.regular,
        fontSize: 17,
        color: colors.green,
    },
    notificationsBoxIcon: {
        alignItems: "center",
        justifyContent: "center",
    },
    notificationsChevron: {
        color: colors.gray,
        fontSize: 16,
    },
    extracstInner: {
        width: "100%",
        borderTopWidth: 1,
        borderTopColor: colors.light_gray,
      },
      wrapDate: {
        justifyContent: "center",
        alignItems: "center",
      },
    
      wrapItems:{
        paddingHorizontal: 29,
      },
      wrapForm:{
        paddingHorizontal: 29,
      },
      input:{
        fontFamily: fonts.regular,
        fontSize: 16,
        color: colors.gray,
        borderBottomWidth: 1,
        borderBottomColor: colors.light_gray,
        paddingVertical: 10,
      }
});
