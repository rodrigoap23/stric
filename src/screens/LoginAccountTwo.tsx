import { Ionicons } from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/core";
import React, { useState } from "react";
import {
  Animated,
  Image,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from "react-native";
import eye from "../../assets/eye.png";
import eyeOff from "../../assets/eyeOff.png";
import logoWhite from "../../assets/logoWhite.png";
import HeaderSection from "../components/HeaderSection";
import colors from "../styles/colors";
import fonts from "../styles/fonts";

export function LoginAccountTwo() {
  const navigation = useNavigation();

  // function handleClose(){
  //     navigation.navigate('Lobby');
  // }
  function handleBack() {
    navigation.navigate("CreateAccountThree");
  }
  function handleNext() {
    navigation.navigate("LoggedIn");
  }
  const [input, setInput] = useState("");
  const [hidePass, setHidePass] = useState(true);
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar />
      <Image source={logoWhite} style={styles.imageLogo} />
      <Animated.View style={styles.content}>
        <HeaderSection
          title={"Digite sua senha de acesso"}
          leftIcon={true}
          leftNavigation={"LoginAccountOne"}
        />

        <View style={styles.inputWrap}>
          <TextInput
            style={styles.inputForm}
            keyboardType="default"
            value={input}
            onChangeText={(text) => setInput(text)}
            secureTextEntry={hidePass}
          />
          <TouchableOpacity onPress={() => setHidePass(!hidePass)}>
            {hidePass ? (
              <Image source={eyeOff} style={styles.eyeOff} />
            ) : (
              <Image source={eye} style={styles.eye} />
            )}
          </TouchableOpacity>
        </View>
        <TouchableOpacity onPress={handleNext} style={styles.wrapButton}>
          <View style={styles.wrapButton}>
            <View style={styles.Button}>
              <Text style={styles.ButtonText}>Continuar</Text>
              <Ionicons name="chevron-forward" style={styles.ButtonIcon} />
            </View>
          </View>
        </TouchableOpacity>
      </Animated.View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    alignItems: "center",
    backgroundColor: colors.dark_blue,
  },
  imageLogo: {
    width: 62,
    height: 23,
    marginTop: 38,
    marginBottom: 38,
  },
  content: {
    flex: 1,
    width: "100%",
    justifyContent: "flex-start",
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: colors.white,
    paddingTop: 40,
    // paddingHorizontal: 29,
  },
  header: {
    // flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: 29,
  },
  title: {
    fontSize: 21,
    fontFamily: fonts.regular,
    color: colors.dark_blue,
    textAlign: "left",
    paddingRight: 60,
    //marginRight:10
  },
  closeIcon: {
    fontSize: 36,
    color: colors.light_blue,
    marginTop: -6,
  },
  form: {
    justifyContent: "center",
    alignItems: "center",
  },
  inputForm: {
    width: "85%",
    color: colors.light_blue,
    fontSize: 30,
    //marginTop: 32,
    fontFamily: "Flexo-Regular",
    borderLeftWidth: 1,
    borderLeftColor: colors.light_gray,
    marginRight: 15,
    // borderWidth: 1,
  },
  inputWrap: {
    paddingVertical: 32,
    paddingHorizontal: 25,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  eyeOff: {
    width: 24,
    height: 19,
  },
  eye: {
    width: 25,
    height: 19,
  },
  wrapButton: {
    flex: 1,
    width: "100%",
    alignItems: "center",
    justifyContent: "flex-end",
  },
  Button: {
    padding: 18,
    paddingHorizontal: 29,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    width: "100%",
    backgroundColor: colors.light_blue,
  },
  ButtonText: {
    color: colors.white,
    fontSize: 22,
    fontFamily: fonts.regular,
  },
  ButtonIcon: {
    fontSize: 36,
    color: colors.white,
    marginTop: -1,
  },
});
