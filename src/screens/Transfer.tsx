import { useNavigation } from "@react-navigation/core";
import React from "react";
import { SafeAreaView, StatusBar, StyleSheet, Text, View } from "react-native";
import music from "../../assets/music.png";
import BoxItem from "../components/BoxItem";
import HeaderSection from "../components/HeaderSection";
import ListItem from "../components/ListItem";
import colors from "../styles/colors";
import fonts from "../styles/fonts";

export function Transfer() {
  const navigation = useNavigation();

  
  function handleBack() {
    navigation.navigate("LoggedIn");
  }
  function handleDepositTwo() {
    // navigation.navigate('DepositTwo');
  }
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar />
      <View style={styles.header}>
        <Text style={styles.userTitle}>Martin Ferramentas</Text>
      </View>
      <View style={styles.content}>
        <HeaderSection
          title={"Escolha uma opção"}
          leftIcon={true}
          leftNavigation={"LoggedIn"}
        />
        <View style={styles.innerContent}>
          <View style={styles.wrapBoxes}>
            <BoxItem
              title={"Transferir" + "\n" + "via Stric"}
              navTo={"TransferStricOne"}
            />
            <BoxItem title={"Transferir" + "\n" + "via Pix"} navTo={"Pix"} />
            <BoxItem
              title={"Transferir" + "\n" + "via TED"}
              navTo={"TransferTedOne"}
            />
          </View>

          <Text style={styles.titleSection}>Últimas transferências</Text>
          <View style={styles.wrapList}>
            <ListItem
              icon={music}
              title={"Transferência via Pix"}
              name={"Adalberto Brito"}
              color={colors.dark_blue}
              values={"-R$ 32,30"}
              colorValue={colors.red}
            />
          </View>
          <View style={styles.wrapList}>
            <ListItem
              icon={music}
              title={"Transferência via TED"}
              name={"Lucas Santos"}
              color={colors.dark_blue}
              values={"-R$ 423,30"}
              colorValue={colors.red}
            />
          </View>
          <View style={styles.wrapList}>
            <ListItem
              icon={music}
              title={"Transferência via Pix"}
              name={"Thiago Luiz"}
              color={colors.dark_blue}
              values={"-R$ 16,90"}
              colorValue={colors.red}
            />
          </View>
          <View style={styles.wrapList}>
            <ListItem
              icon={music}
              title={"Transferência via TED"}
              name={"Alex Figueiredo"}
              color={colors.dark_blue}
              values={"-R$ 1678,00"}
              colorValue={colors.red}
            />
          </View>
          <View style={styles.wrapList}>
            <ListItem
              icon={music}
              title={"Transferência via Pix"}
              name={"Patricia Santana"}
              color={colors.dark_blue}
              values={"-R$ 3.266,30"}
              colorValue={colors.red}
            />
          </View>
          <View style={styles.wrapList}>
            <ListItem
              icon={music}
              title={"Transferência via TED"}
              name={"Fernanda Fernandes"}
              color={colors.dark_blue}
              values={"-R$ 2.220,90"}
              colorValue={colors.red}
            />
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    alignItems: "center",
    backgroundColor: colors.dark_blue,
    justifyContent: "center",
  },
  header: {
    paddingVertical: 34,
  },
  userTitle: {
    fontFamily: fonts.bold,
    color: colors.white,
    fontSize: 18,
    textAlign: "center",
  },
  content: {
    flex: 1,
    width: "100%",
    justifyContent: "flex-start",
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: colors.white,
    paddingTop: 40,
  },
  innerContent: {
    paddingHorizontal: 29,
    paddingTop: 20,
  },
  wrapBoxes: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingVertical: 20,
  },
  titleSection: {
    fontFamily: fonts.medium,
    color: colors.dark_blue,
    fontSize: 14,
    width: "100%",
    textAlign: "center",
    paddingVertical: 20,
  },
  wrapList: {},
});
