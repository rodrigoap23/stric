import { useNavigation } from "@react-navigation/core";
import React,{useState} from "react";
import {
    Button,
    SafeAreaView,
    StatusBar,
    StyleSheet,
    Text, TouchableOpacity, View
} from "react-native";
import HeaderSection from "../components/HeaderSection";
import ListItem from "../components/ListItem";
import colors from "../styles/colors";
import fonts from "../styles/fonts";
import Modal from "react-native-modal";
import { Ionicons } from "@expo/vector-icons";
import { color } from "react-native-reanimated";



export function Users() {
    const [visible, setVisible] = useState(false);


    const navigation = useNavigation();

    function handleLnk(lnk: string) { }
    return (
        <>
        <SafeAreaView style={styles.container}>
            <StatusBar />
            <View style={styles.header}>
                <Text style={styles.userTitle}>Martin Ferramentas</Text>
            </View>
            <View style={styles.content}>
                <HeaderSection 
                    leftIcon={true}
                    leftNavigation={"Account"}
                    rightIcon={false}
                    rightNavigation={"LoggedIn"}    
                    title={"Operadores(4)"}
                    addButton={"AddUser"}
                    
                    

                />
                
                <View style={styles.wrapNotifications}>
                    
                      
                        <ListItem
                            useInitials={true}
                            icon={"AB"}
                            title={"Operador"}
                            name={"Adalberto Brito"}
                            color={colors.dark_blue}
                            editItem={true}
                            deleteFunction={() => {setVisible(true)}}
                            editFunction={() => {navigation.navigate("EditUser")}}
                        />
                   
                     
                        <ListItem
                            useInitials={true}
                            icon={"FS"}
                            title={"Operador"}
                            name={"Fernando Sales"}
                            color={colors.dark_blue}
                            editItem={true}
                        />
                    
                   
                        <ListItem
                            useInitials={true}
                            icon={"OT"}
                            title={"Operador"}
                            name={"Olívia Tereza"}
                            color={colors.dark_blue}
                            editItem={true}
                        />
                   
                   
                    <ListItem
                        useInitials={true}
                        icon={"KB"}
                        title={"Operador"}
                        name={"Kleber Basilio"}
                        color={colors.dark_blue}
                        editItem={true}
                    />
                    

                </View>
                
            </View>
           
        </SafeAreaView>
         <View style={{flex:1, justifyContent:"center", alignItems:"center"}}>
            <Modal backdropColor="white" backdropOpacity={0} isVisible={visible} style={styles.modal} >
               <View style={styles.modalContent}>
                <Text style={styles.modalText}>
                    Você tem certeza que{'\n'} deseja excluir o operador?
                </Text>
                
                <View style={styles.modalButton}>
                    <TouchableOpacity onPress={()=>{setVisible(false)}} style={styles.wrapButton}>
                        <View style={[styles.buttonInner, {backgroundColor: colors.light_blue}]}>
                            <View style={styles.Button}>
                            <Text style={styles.ButtonText}>Não</Text>
                            <Ionicons name="chevron-forward" style={styles.ButtonIcon} />
                            </View>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>{setVisible(false)}} style={styles.wrapButton}>
                        <View style={[styles.buttonInner, {backgroundColor: colors.red}]}>
                            <View style={styles.Button}>
                            <Text style={styles.ButtonText}>Sim</Text>
                            <Ionicons name="close" style={styles.ButtonIcon} />
                            </View>
                        </View>
                    </TouchableOpacity>
                </View> 
               </View>

               

            </Modal>
        </View>
        </>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: "100%",
        alignItems: "center",
        backgroundColor: colors.dark_blue,
    },
    header: {
        paddingVertical: 34,
    },
    userTitle: {
        fontFamily: fonts.bold,
        color: colors.white,
        fontSize: 18,
        textAlign: "center",
    },
    content: {
        flex: 1,
        width: "100%",
        justifyContent: "flex-start",
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15,
        backgroundColor: colors.white,
        paddingTop: 40,
    },
    wrapNotifications: {
        paddingBottom: 20,
        paddingHorizontal: 29,
        paddingTop: 30,
    },
    value: {
        fontFamily: fonts.regular,
        fontSize: 17,
    },
    valuePositive: {
        fontFamily: fonts.regular,
        fontSize: 17,
        color: colors.green,
    },
    notificationsBoxIcon: {
        alignItems: "center",
        justifyContent: "center",
    },
    notificationsChevron: {
        color: colors.gray,
        fontSize: 16,
    },
    extracstInner: {
        width: "100%",
        borderTopWidth: 1,
        borderTopColor: colors.light_gray,
      },
      wrapDate: {
        justifyContent: "center",
        alignItems: "center",
      },
    
      wrapItems:{
        paddingHorizontal: 29,
      },
      wrapForm:{
        paddingHorizontal: 29,
      },
      input:{
        fontFamily: fonts.regular,
        fontSize: 16,
        color: colors.gray,
        borderBottomWidth: 1,
        borderBottomColor: colors.light_gray,
        paddingVertical: 10,
      },
      modal:{
          flex:1,
          padding:0,
          marginHorizontal:0,
          marginBottom:0,
          marginTop:90,
          borderTopEndRadius:15,
          borderTopStartRadius:15,
          backgroundColor: 'rgba(255,255,255,0.8)',
      },
      modalContent:{
            flex:1,
          justifyContent:"flex-end",
      },
      modalText:{
            fontFamily: fonts.regular,
            fontSize: 26,
            textAlign: "center",
      },
      modalButton:{
        flexDirection: "row",
        justifyContent: "space-between", 
        marginTop:60
      },
      wrapButton:{
        width: "50%",
        
      },
      buttonInner:{
        paddingVertical:18,
        paddingHorizontal:28,
      },
      Button:{
          flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
            
      },
      ButtonText:{
        fontFamily: fonts.regular,
        fontSize: 22,
        color: colors.white,
      },
      ButtonIcon:{
        fontSize: 22,
        color: colors.white,
      }
      
});
