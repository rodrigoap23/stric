import React, { useState } from "react";
import {
  Image,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { TextInput } from "react-native-gesture-handler";
import { TextInputMask } from "react-native-masked-text";
import eye from "../../assets/eye.png";
import ConfirmButton from "../components/ConfirmButton";
import HeaderSection from "../components/HeaderSection";
import colors from "../styles/colors";
import fonts from "../styles/fonts";

export function TransfTedThree(props: any) {
  const [money, setMoney] = useState(props.route.params.paramValue);
  const [isEnabled, setIsEnabled] = useState(true);
  const toggleSwitch = () => setIsEnabled((previousState) => !previousState);

  return (
    <>
      <ScrollView>
        <SafeAreaView style={styles.container}>
          <StatusBar />
          <View style={styles.header}>
            <Text style={styles.userTitle}>Martin Ferramentas</Text>
          </View>
          <View style={styles.content}>
            <HeaderSection
              title="Revisão"
              leftIcon={true}
              leftNavigation={"TransfTedTwo"}
              rightIcon={false}
              rightNavigation={"LoggedIn"}
            />

            <View style={styles.wrapInner}>
              <View style={styles.wrapBalance}>
                <Text style={styles.balanceTitle}>Saldo:</Text>
                <Text style={styles.balanceValue}>R$ 24.895,36</Text>
                <TouchableOpacity>
                  <Image style={styles.icon} source={eye} />
                </TouchableOpacity>
              </View>
              <View style={styles.wrapDepositValue}>
                <Text style={styles.depositFormLabel}>Valor do boleto:</Text>
                <View>
                  <TextInputMask
                    style={styles.inputValue}
                    type="money"
                    options={{
                      precision: 2,
                      separator: ",",
                      delimiter: ".",
                      unit: "R$ ",
                    }}
                    value={money}
                    onChangeText={(text) => setMoney(text)}
                    editable={false}
                  />
                </View>
              </View>
            </View>
            <View style={styles.wrapForm}>
              <View style={styles.wrapRowOne}>
                <Text style={styles.label}>Nome do banco</Text>
                <TextInput
                  editable={false}
                  value={"033 Santander Banespa"}
                  style={styles.input}
                />
              </View>
              <View style={styles.wrapRowTwo}>
                <View style={styles.wrapInputCol}>
                  <Text style={styles.label}>Agência</Text>
                  <TextInput
                    editable={false}
                    value={"1245"}
                    style={styles.input}
                  />
                </View>
                <View style={styles.wrapInputCol}>
                  <Text style={styles.label}>Conta</Text>
                  <TextInput
                    editable={false}
                    value={"124482991-2"}
                    style={styles.input}
                  />
                </View>
              </View>
              <View style={styles.wrapRowThree}>
                <View style={styles.wrapInput}>
                  <Text style={styles.label}>Nome do favorecido</Text>
                  <TextInput
                    editable={false}
                    value={props.route.params.paramUser}
                    style={styles.input}
                  />
                </View>
              </View>
              <View style={styles.wrapRowThree}>
                <View style={styles.wrapInput}>
                  <Text style={styles.label}>CPF/CNPJ</Text>
                  <TextInput
                    editable={false}
                    value={"728.293.241-24"}
                    style={styles.input}
                  />
                </View>
              </View>
              <View style={styles.wrapRowOne}>
                <Text style={styles.label}>Tipo de conta</Text>
                <TextInput
                  editable={false}
                  value={"Corrente"}
                  style={styles.input}
                />
              </View>
              <View style={styles.wrapInput}>
                <Text style={styles.label}>Descrição{"\n\n\n"}-</Text>
                <TextInput editable={false} style={styles.input} />
              </View>
            </View>
            <View style={styles.wrapFooter}>
              <View style={styles.wrapInner}>
                <Text style={styles.text}>
                  Transferir Hoje ou Próximo dia útil
                </Text>
              </View>
              <Text style={styles.warnLabel}>
                O custo dessa operação é de R$4,90
              </Text>
            </View>
          </View>
        </SafeAreaView>
      </ScrollView>

      <ConfirmButton title="Continuar" navTo={"TransfTedAuth"} />
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    alignItems: "center",
    backgroundColor: colors.dark_blue,
  },
  header: {
    paddingVertical: 34,
  },
  userTitle: {
    fontFamily: fonts.bold,
    color: colors.white,
    fontSize: 18,
    textAlign: "center",
  },
  content: {
    flex: 1,
    width: "100%",
    justifyContent: "flex-start",
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: colors.white,
    paddingTop: 40,
  },
  wrapInner: {
    width: "100%",
    paddingHorizontal: 29,
    paddingTop: 20,
  },
  wrapBalance: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    borderWidth: 1,
    borderColor: colors.light_gray,
    borderRadius: 10,
    paddingHorizontal: 20,
    paddingVertical: 15,
  },
  balanceTitle: {
    fontFamily: fonts.regular,
    fontSize: 18,
    color: colors.dark_blue,
  },
  balanceValue: {
    fontFamily: fonts.bold,
    fontSize: 18,
    color: colors.dark_blue,
  },
  icon: {
    width: 24,
    height: 18,
    resizeMode: "contain",
  },
  wrapDepositValue: {
    alignItems: "center",
    paddingVertical: 20,
  },
  depositFormLabel: {
    fontFamily: fonts.regular,
    fontSize: 18,
    color: colors.dark_blue,
    marginBottom: 10,
  },
  inputValue: {
    fontFamily: fonts.regular,
    fontSize: 32,
    color: colors.light_blue,

    paddingBottom: 10,
  },
  wrapForm: {
    flex: 1,
    justifyContent: "flex-start",
    paddingHorizontal: 29,
    paddingTop: 10,
  },
  label: {
    fontFamily: fonts.regular,
    fontSize: 14,
    color: colors.dark_blue,
  },
  wrapInput: {},
  wrapInputCol: {
    width: "48%",
  },
  wrapRowOne: {
    width: "100%",
    padding: 0,

    marginBottom: 34,
  },
  wrapRowTwo: {
    flexDirection: "row",
    justifyContent: "space-between",
    padding: 0,
    marginBottom: 34,
  },
  wrapRowThree: {
    marginBottom: 34,
  },
  input: {
    fontFamily: fonts.regular,
    fontSize: 16,
    color: colors.gray,

    paddingVertical: 5,
  },
  sectionFooter: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingVertical: 34,
    alignItems: "center",
  },

  sectionLabel: {
    fontFamily: fonts.regular,
    fontSize: 14,
    color: colors.light_blue,
    marginRight: 10,
  },
  containerSwitch: {},
  wrapBoxes: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: 29,
    paddingVertical: 20,
  },
  wrapFooter: {
    alignItems: "center",
  },

  warnLabel: {
    fontFamily: fonts.regular,
    color: colors.gray,
    fontSize: 14,
    paddingBottom: 33,
    paddingTop: 70,
  },
  text: {
    fontFamily: fonts.regular,
    color: colors.dark_blue,
    fontSize: 14,
  },
});
