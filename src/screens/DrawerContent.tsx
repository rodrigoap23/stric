import React from "react";
import { SafeAreaView, StatusBar, StyleSheet, Text, View } from "react-native";
import {DrawerContentScrollView, DrawerItem} from "@react-navigation/drawer";
import { Ionicons } from "@expo/vector-icons";
import { TouchableOpacity } from "react-native-gesture-handler";
import { DrawerActions } from "@react-navigation/native";
import { useNavigation } from "@react-navigation/native";

import colors from "../styles/colors";
import fonts from "../styles/fonts";
export function DrawerContent(props:any) {

    function handleCloseMenu(){
        navigation.dispatch(DrawerActions.closeDrawer())
    }
  function   handleNavigation(route:string){
        navigation.navigate(route)
    }
    


    const navigation = useNavigation();
    return(
       <View style={styles.container}>
           <View style={styles.header}>
               <Text style={styles.title}>
                    Agência 0001 Conta 23345192-5{'\n'}
                    Banco 123 - Stric Pagamentos S.A.
               </Text>
               <TouchableOpacity onPress={handleCloseMenu}>
                <Ionicons name="close" style={styles.closeIcon}/>
               </TouchableOpacity>
           </View>
           <View style={styles.innerContent}>
               <View style={styles.wrapItem}>
                <TouchableOpacity 
                    onPress={()=>{handleNavigation("Transfer"), handleCloseMenu()}}
                    style={styles.item}
                >
                    <Text style={styles.itemTitle}>
                        Transferencias
                    </Text>
                    <Ionicons name="chevron-forward" style={styles.itemIcon}/>
                </TouchableOpacity>
               </View>
               <View style={styles.wrapItem}>
                <TouchableOpacity 
                onPress={()=>{handleNavigation("Pix"), handleCloseMenu()}}
                style={styles.item}>
                    <Text style={styles.itemTitle}>
                        Pix
                    </Text>
                    <Ionicons name="chevron-forward" style={styles.itemIcon}/>
                </TouchableOpacity>
               </View>
               <View style={styles.wrapItem}>
                <TouchableOpacity 
                onPress={()=>{handleNavigation("DepositOne"), handleCloseMenu()}}
                style={styles.item}>

                
                    <Text style={styles.itemTitle}>
                        Depósito por boleto
                    </Text>
                    <Ionicons name="chevron-forward" style={styles.itemIcon}/>
                </TouchableOpacity>
               </View>
               <View style={styles.wrapItem}>
                <TouchableOpacity 
                onPress={()=>{handleNavigation("PayOne"), handleCloseMenu()}}
                style={styles.item}>
                    <Text style={styles.itemTitle}>
                        Pagar contas
                    </Text>
                    <Ionicons name="chevron-forward" style={styles.itemIcon}/>
                </TouchableOpacity>
               </View>
               <View style={styles.wrapItem}>
                <TouchableOpacity 
                onPress={()=>{handleNavigation("DrawerContent"), handleCloseMenu()}}
                style={styles.item}>
                    <Text style={styles.itemTitle}>
                        Extrato
                    </Text>
                    <Ionicons name="chevron-forward" style={styles.itemIcon}/>
                </TouchableOpacity>
               </View>
               <View style={styles.wrapItem}>
                <TouchableOpacity 
                onPress={()=>{handleNavigation("Charge"), handleCloseMenu()}}
                style={styles.item}>
                    <Text style={styles.itemTitle}>
                        Cobranças
                    </Text>
                    <Ionicons name="chevron-forward" style={styles.itemIcon}/>
                </TouchableOpacity>
               </View>
               <View style={styles.wrapItem}>
                <TouchableOpacity 
                onPress={()=>{handleNavigation("Account"), handleCloseMenu()}}
                style={styles.item}>
                    <Text style={styles.itemTitle}>
                        Conta
                    </Text>
                    <Ionicons name="chevron-forward" style={styles.itemIcon}/>
                </TouchableOpacity>
               </View>
           </View>
       </View>
    )

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "flex-start",
        paddingHorizontal:29,
        paddingTop:10,
    },
    header:{
        flexDirection:"row",
        justifyContent:"space-between",
        alignItems:"center",
        paddingBottom:35
    },
    title:{
        fontFamily: fonts.regular,
        fontSize: 14,
        color: colors.white,
    },
    closeIcon:{
        fontSize: 36,
        color: colors.light_blue,
    },
    innerContent:{
       
    },
    wrapItem:{
        paddingVertical:25,
        borderBottomWidth:1,
        borderBottomColor:colors.blue,
    },
    item:{
        flexDirection:"row",
        justifyContent:"space-between",
        alignItems:"center",
    },
    itemTitle:{
        fontFamily: fonts.regular,
        fontSize: 16,
        color: colors.white,
    },
    itemIcon:{
        color: colors.white,
        fontSize: 20,
    }
})