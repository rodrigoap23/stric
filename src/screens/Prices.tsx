import React from "react";
import {
    SafeAreaView,
    StatusBar,
    StyleSheet,
    Text,
    View
} from "react-native";
import HeaderSection from "../components/HeaderSection";
import colors from "../styles/colors";
import fonts from "../styles/fonts";

export function Prices() {
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar />
      <View style={styles.header}>
        <Text style={styles.userTitle}>Martin Ferramentas</Text>
      </View>
      <View style={styles.content}>
        <View style={styles.headerSection}>
          <HeaderSection
            title={"Tarifas e Limites"}
            leftIcon={true}
            leftNavigation={"Account"}
          />
        </View>

        <View style={styles.wrapContent}>
          <Text style={styles.title}>Tarifas</Text>
          <View style={styles.wrapInner}>
            <Text style={styles.text}>Abertura de conta</Text>
            <Text style={styles.textValue}>R$ 0,00</Text>
          </View>
          <View style={styles.wrapInner}>
            <Text style={styles.text}>Manutenção de conta</Text>
            <Text style={styles.textValue}>R$ 0,00</Text>
          </View>
          <View style={styles.wrapInner}>
            <Text style={styles.text}>Encerramento de conta</Text>
            <Text style={styles.textValue}>R$ 0,00</Text>
          </View>
          <View style={styles.wrapInner}>
            <Text style={styles.text}>Emissão de cartão</Text>
            <Text style={styles.textValue}>R$ 0,00</Text>
          </View>
          <View style={styles.wrapInner}>
            <Text style={styles.text}>Anuidade de cartão</Text>
            <Text style={styles.textValue}>R$ 0,00</Text>
          </View>
          <View style={styles.wrapInner}>
            <Text style={styles.text}>Transferências entre contas Stric</Text>
            <Text style={styles.textValue}>R$ 0,00</Text>
          </View>
          <View style={styles.wrapInner}>
            <Text style={styles.text}>Demais Tranferências</Text>
            <Text style={styles.textValue}>R$ 0,00</Text>
          </View>
          <View style={styles.wrapInner}>
            <Text style={styles.text}>Compas internacionais</Text>
            <Text style={styles.textValue}>4% + 6,38% IOF</Text>
          </View>
        </View>
        <View style={styles.wrapContent}>
          <Text style={styles.title}>Limites</Text>
          <View style={styles.wrapInner}>
            <Text style={styles.text}>Entre contas Stric</Text>
            <Text style={styles.textValue}>R$ 1.000.00,00</Text>
          </View>
          <View style={styles.wrapInner}>
            <Text style={styles.text}>Mesmo CNPJ</Text>
            <Text style={styles.textValue}>R$ 1.000.00,00</Text>
          </View>
          <View style={styles.wrapInner}>
            <Text style={styles.text}>Para outros bancos</Text>
            <Text style={styles.textValue}>R$ 100.000,00</Text>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    alignItems: "center",
    backgroundColor: colors.dark_blue,
    justifyContent: "center",
  },
  header: {
    paddingVertical: 34,
  },
  headerSection: {
    paddingBottom: 30,
  },
  userTitle: {
    fontFamily: fonts.bold,
    color: colors.white,
    fontSize: 18,
    textAlign: "center",
  },

  content: {
    flex: 1,
    width: "100%",
    justifyContent: "flex-start",
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: colors.white,
    paddingTop: 40,
  },
  wrapContent: {
    paddingHorizontal: 29,
  },

  title: {
    fontFamily: fonts.bold,
    color: colors.dark_blue,
    fontSize: 16,
    marginTop: 30,
    paddingBottom: 15,
  },
  wrapInner: {
    flexDirection: "row",
    marginVertical: 7,
  },
  text: {
    flex: 1,
    fontFamily: fonts.regular,
    color: colors.dark_blue,
    fontSize: 14,
  },
  textValue: {
    fontFamily: fonts.regular,
    color: colors.light_blue,
    fontSize: 14,
  },
});
