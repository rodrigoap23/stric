import { useNavigation } from "@react-navigation/core";
import { BarCodeScanner } from "expo-barcode-scanner";
import React, { useEffect, useState } from "react";
import {
  Button,
  Dimensions,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import ConfirmButton from "../components/ConfirmButton";
import colors from "../styles/colors";
import fonts from "../styles/fonts";

export function PayOne() {
  const navigation = useNavigation();

  const [hasPermission, setHasPermission] = useState(true);
  const [scanned, setScanned] = useState(false);

  useEffect(() => {
    (async () => {
      const { status } = await BarCodeScanner.requestPermissionsAsync();
      setHasPermission(status === "granted");
    })();
  }, []);

  const handleBarCodeScanned = ({}) => {
    setScanned(true);
    navigation.navigate("PixThree");
  };

  if (hasPermission === null) {
    return <Text>Requesting for camera permission</Text>;
  }
  if (hasPermission === false) {
    return <Text>No access to camera</Text>;
  }

  return (
    <View style={styles.container}>
      <BarCodeScanner
        onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
        style={StyleSheet.absoluteFillObject}
      />
      {scanned && (
        <Button
          title={"Toque para Scannear outra vez!"}
          onPress={() => setScanned(false)}
        />
      )}
      <>
        <View style={styles.wrapHeader}>
          <View style={styles.headerTitle}>
            <Text style={styles.title}>Martin Ferramentas</Text>
          </View>
          <View>
            <TouchableOpacity
              onPress={() => {
                navigation.navigate("LoggedIn");
              }}
            >
              <Text style={styles.navButtonText}>Cancelar</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.wrapContent}>
          <View style={styles.wrapQrScanner}></View>
          <Text style={styles.textContent}>
            Aponte sua câmera{"\n"}
            para um Pix QR code
          </Text>
        </View>
        <View>
          <ConfirmButton
            title={"Pagar com códido de barras"}
            navTo={"PayTwo"}
          />
        </View>
      </>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.dark_blue,
    flex: 1,
  },
  wrapHeader: {
    backgroundColor: colors.dark_blue,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingHorizontal: 29,
    paddingVertical: 43,
  },
  headerTitle: {
    flex: 1,
    paddingLeft: 45,
  },
  title: {
    fontFamily: fonts.bold,
    fontSize: 18,
    color: colors.white,
    textAlign: "center",
  },
  navButtonText: {
    fontFamily: fonts.regular,
    fontSize: 16,
    color: colors.light_blue,
  },

  wrapContent: {
    flex: 1,
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
  },
  textContent: {
    fontFamily: fonts.regular,
    fontSize: 18,
    color: colors.white,
    marginTop: 44,
  },
  wrapQrScanner: {
    width: Dimensions.get("screen").width / 1.5,
    height: Dimensions.get("screen").width / 1.5,
    borderWidth: 2,
    borderColor: colors.white,
    borderRadius: 12,
  },
  scan: {
    width: Dimensions.get("screen").width / 1.5,
  },
});
