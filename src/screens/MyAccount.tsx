import { Picker } from "@react-native-community/picker";
import { useNavigation } from "@react-navigation/core";
import React, { useState } from "react";
import {
  Image,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Switch,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { TextInput } from "react-native-gesture-handler";
import { TextInputMask } from "react-native-masked-text";
import eye from "../../assets/eye.png";
import ConfirmButton from "../components/ConfirmButton";
import HeaderSection from "../components/HeaderSection";
import colors from "../styles/colors";
import fonts from "../styles/fonts";

export function MyAccount() {
  const [money, setMoney] = useState("0,00");
  const [isEnabled, setIsEnabled] = useState(true);
  const toggleSwitch = () => setIsEnabled((previousState) => !previousState);

  const navigation = useNavigation();


  return (
    <>
      <ScrollView>
        <SafeAreaView style={styles.container}>
          <StatusBar />
          <View style={styles.header}>
            <Text style={styles.userTitle}>Martin Ferramentas</Text>
          </View>
          <View style={styles.content}>
            <HeaderSection
              title="Minha Conta"
              leftIcon={true}
              leftNavigation={"Account"}
              rightIcon={false}
              rightNavigation={"LoggedIn"}
            />

            <View style={styles.wrapInner}>
            </View>
            <View style={styles.wrapForm}>
              <View style={styles.wrapRowOne}>
                <Text style={styles.label}>Sexo</Text>
                <Picker style={styles.input}>
                  <Picker.Item label="Masculino" value="1" />
                  <Picker.Item label="Feminino" value="1" />
                </Picker>
              </View>
              <View style={styles.wrapRowThree}>
                <View style={styles.wrapInputCol}>
                  <Text style={styles.label}>Celular</Text>
                  <TextInput
                    
                    value={"(11) 99999-9999"}
                    style={styles.input}
                  />
                </View>
              </View>
              <View style={styles.wrapRowThree}>
                <View style={styles.wrapInput}>
                  <Text style={styles.label}>Cep</Text>
                  <TextInput
                    value={"13012-291"}
                    style={styles.input}
                    keyboardType="numeric"  
                  />
                </View>
              </View>
              <View style={[styles.wrapRowThree, {flexDirection:"row", justifyContent:"space-between"}]}>
                <View style={[styles.wrapInput, {width: "75%"}]}>
                  <Text style={styles.label}>Endereço</Text>
                  <TextInput
                    value={"Av. Do Ipiranga"}
                    style={styles.input}
                  />
                </View>
                <View style={[styles.wrapInput, {width: "20%"}]}>
                  <Text style={styles.label}>Nº</Text>
                  <TextInput
                    value={"231"}
                    style={styles.input}
                    keyboardType="numeric"
                  />
                </View>
              </View>
              <View style={styles.wrapRowThree}>
              <View style={styles.wrapInput}>
                <Text style={styles.label}>Complemento</Text>
                <TextInput editable={false} style={styles.input} />
              </View>
              </View>
              <View style={styles.wrapRowThree}>
              <View style={styles.wrapInput}>
                <Text style={styles.label}>Bairro</Text>
                <TextInput editable={false} value={"Itaim Bibi"} style={styles.input} />
              </View>
              </View>
             

            </View>
            {/* <View style={styles.wrapFooter}>
             
             
            </View> */}
          </View>
        </SafeAreaView>
      </ScrollView>

      <ConfirmButton title="Salvar" navTo={"Account"} />
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    alignItems: "center",
    backgroundColor: colors.dark_blue,
  },
  header: {
    paddingVertical: 34,
  },
  userTitle: {
    fontFamily: fonts.bold,
    color: colors.white,
    fontSize: 18,
    textAlign: "center",
  },
  headerContent: {
    //flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  content: {
    flex: 1,
    width: "100%",
    justifyContent: "flex-start",
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: colors.white,
    paddingTop: 40,
  },
  wrapInner: {
    paddingHorizontal: 29,
    paddingTop: 20,
  },
  titleContent: {
    fontSize: 21,
    fontFamily: fonts.regular,
    color: colors.dark_blue,
    textAlign: "left",
    paddingRight: 180,
  },
  backIcon: {
    fontSize: 36,
    color: colors.light_blue,
  },
  cancelButton: {
    fontFamily: fonts.regular,
    color: colors.light_blue,
    fontSize: 16,
  },
  wrapBalance: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    borderWidth: 1,
    borderColor: colors.light_gray,
    borderRadius: 10,
    paddingHorizontal: 20,
    paddingVertical: 15,
  },
  balanceTitle: {
    fontFamily: fonts.regular,
    fontSize: 18,
    color: colors.dark_blue,
  },
  balanceValue: {
    fontFamily: fonts.bold,
    fontSize: 18,
    color: colors.dark_blue,
  },
  icon: {
    width: 24,
    height: 18,
    resizeMode: "contain",
  },
  wrapDepositValue: {
    alignItems: "center",
    paddingVertical: 20,
  },
  depositFormLabel: {
    fontFamily: fonts.regular,
    fontSize: 18,
    color: colors.dark_blue,
    marginBottom: 10,
  },
  inputValue: {
    fontFamily: fonts.regular,
    fontSize: 32,
    color: colors.light_blue,
    borderBottomWidth: 1,
    borderBottomColor: colors.light_gray,
    paddingBottom: 10,
  },
  wrapForm: {
    flex: 1,
    justifyContent: "flex-start",
    paddingHorizontal: 29,
    paddingTop: 10,
  },
  label: {
    fontFamily: fonts.regular,
    fontSize: 14,
    color: colors.dark_blue,
  },
  wrapInput: {},
  wrapInputCol: {
    width: "100%",
  },
  wrapRowOne: {
    width: "100%",
    padding: 0,
    borderBottomWidth: 1,
    borderBottomColor: colors.light_gray,
    marginBottom: 34,
  },
  wrapRowTwo: {
    flexDirection: "row",
    justifyContent: "space-between",
    padding: 0,
    marginBottom: 34,
  },
  wrapRowThree: {
    marginBottom: 34,
  },
  input: {
    fontFamily: fonts.regular,
    fontSize: 16,
    color: colors.gray,
    borderBottomWidth: 1,
    borderBottomColor: colors.light_gray,
    paddingVertical: 5,
  },
  sectionFooter: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingVertical: 34,
    alignItems: "center",
  },
  wrapSwitch: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  sectionLabel: {
    fontFamily: fonts.regular,
    fontSize: 14,
    color: colors.light_blue,
    marginRight: 10,
  },
  containerSwitch: {},
  wrapBoxes: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: 29,
    paddingVertical: 20,
  },
  wrapFooter: {
    alignItems: "center",
  },
  box: {
    width: "50%",
    height: 88,
    borderRadius: 12,
    justifyContent: "space-between",
    paddingLeft: 23,
    paddingBottom: 20,

    backgroundColor: "#fff",
    shadowColor: "#D8D8D8",
    borderWidth: 0,
    borderColor: colors.light_gray,
    borderBottomWidth: 0,
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.3,
    shadowRadius: 2,
    elevation: 9,
  },
  boxActive: {
    width: "50%",
    height: 88,
    borderRadius: 12,
    justifyContent: "space-between",
    paddingLeft: 23,
    paddingBottom: 15,
    borderWidth: 5,
    borderColor: colors.light_blue,
  },
  wrapTile: {
    justifyContent: "center",
    alignItems: "center",
  },
  tile: {
    width: 39,
    height: 4,
    backgroundColor: colors.light_blue,
    borderRadius: 12,
  },
  titleBox: {
    fontFamily: fonts.regular,
    fontSize: 14,
    color: colors.dark_blue,
  },
  warnLabel: {
    fontFamily: fonts.regular,
    color: colors.gray,
    fontSize: 14,
    paddingVertical: 33,
  },
  wrapButton: {
    width: "100%",
    alignItems: "center",
    justifyContent: "flex-end",
  },
  Button: {
    padding: 18,
    flexDirection: "row",
    justifyContent: "space-between",
    width: "100%",
    alignItems: "center",
    backgroundColor: colors.light_blue,
  },
  ButtonText: {
    color: colors.white,
    fontSize: 22,
    fontFamily: fonts.regular,
  },
  ButtonIcon: {
    fontSize: 16,
    color: colors.white,
    marginTop: 0,
  },
});
