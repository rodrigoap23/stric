import React, { useState } from "react";
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  View,
} from "react-native";
import stricsuccess from "../../assets/stricsuccess.png";
import ConfirmButton from "../components/ConfirmButton";
import HeaderTransferSection from "../components/HeaderTransferSection";
import colors from "../styles/colors";
import fonts from "../styles/fonts";

export function TransfTedSuccess(props: any) {
  const [isEnabled, setIsEnabled] = useState(true);
  const toggleSwitch = () => setIsEnabled((previousState) => !previousState);

  return (
    <>
      <ScrollView>
        <SafeAreaView style={styles.container}>
          <StatusBar />
          <View style={styles.header}>
            <Text style={styles.userTitle}>Martin Ferramentas</Text>
          </View>
          <View style={styles.content}>
            <HeaderTransferSection
              sectionImage={stricsuccess}
              title="Transferência Realizada"
              value={"2.500,00"}
              leftNavigation={"TransferTedOne"}
            />
            <View style={styles.wrapInner}>
              <View style={styles.wrap}>
                <Text style={styles.label}>ID da transação</Text>
                <Text style={styles.text}>
                  KSJJFI28AHHSPQOSJL3HSIS3EHS839374EEU
                </Text>
              </View>
              <View style={styles.wrapHalfColumn}>
                <View style={styles.wrapCol}>
                  <Text style={styles.label}>Pagamento</Text>
                  <Text style={styles.text}>12/12/2004</Text>
                </View>
                <View style={styles.wrapCol}>
                  <Text style={styles.label}>Hora</Text>
                  <Text style={styles.text}>14:40</Text>
                </View>
              </View>
              <View style={styles.wrap}>
                <Text style={styles.label}>Descrição</Text>
                <Text style={styles.text}>Dívida antiga</Text>
              </View>
              <Text style={styles.sectionTitle}>Quem recebeu?</Text>
              <View style={styles.wrapHalfColumn}>
                <View style={styles.wrap}>
                  <Text style={styles.label}>Nome</Text>
                  <Text style={styles.text}>Fernanda Prato</Text>
                </View>
                <View style={styles.wrap}>
                  <Text style={styles.label}>CPF/CNPJ</Text>
                  <Text style={styles.text}>728.293.241-24</Text>
                </View>
              </View>
              <Text style={styles.sectionTitle}>Quem recebeu?</Text>

              <View style={styles.wrapHalfColumn}>
                <View style={styles.wrap}>
                  <Text style={styles.label}>Nome</Text>
                  <Text style={styles.text}>Marcela Angelina</Text>
                </View>
                <View style={styles.wrap}>
                  <Text style={styles.label}>CPF/CNPJ</Text>
                  <Text style={styles.text}>431.124.256-12</Text>
                </View>
              </View>

              <View style={styles.wrap}>
                <Text style={styles.label}>Banco</Text>
                <Text style={styles.text}>033 Santander Banespa</Text>
              </View>
            </View>
          </View>
        </SafeAreaView>
      </ScrollView>

      <ConfirmButton title="Compartilhar" navTo={"LoggedIn"} />
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    alignItems: "center",
    backgroundColor: colors.dark_blue,
  },
  header: {
    paddingVertical: 34,
  },
  userTitle: {
    fontFamily: fonts.bold,
    color: colors.white,
    fontSize: 18,
    textAlign: "center",
  },
  content: {
    flex: 1,
    width: "100%",
    justifyContent: "flex-start",
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: colors.white,
    paddingTop: 24,
    paddingHorizontal: 29,
  },
  wrapInner: {
    marginBottom: 95,
  },
  input: {
    width: "100%",
    textAlign: "center",
    fontSize: 50,
    fontFamily: fonts.medium,
    color: colors.dark_blue,
  },
  label: {
    fontFamily: fonts.regular,
    fontSize: 14,
    color: colors.dark_blue,
  },
  text: {
    fontFamily: fonts.regular,
    fontSize: 14,
    color: colors.gray,
  },
  sectionTitle: {
    fontFamily: fonts.bold,
    fontSize: 16,
    color: colors.light_blue,
    marginBottom: 8,
  },
  wrapHalfColumn: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "flex-start",
  },
  wrap: {
    paddingVertical: 24,
    flex: 1,
  },
  wrapCol: {
    paddingVertical: 24,
    flex: 1,
  },
});
