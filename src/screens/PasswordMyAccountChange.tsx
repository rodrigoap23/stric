import { Ionicons } from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/core";
import React, { useState } from "react";
import {
    Animated,
    Image,
    SafeAreaView,
    StatusBar,
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    View,
} from "react-native";
import eye from "../../assets/eye.png";
import eyeOff from "../../assets/eyeOff.png";
import logoWhite from "../../assets/logoWhite.png";
import HeaderSection from "../components/HeaderSection";
import colors from "../styles/colors";
import fonts from "../styles/fonts";

export function PasswordMyAccountChange() {
    const navigation = useNavigation();


    function handleOk() {
        alert("Senha alterada com sucesso!");
    }
    
    const [input, setInput] = useState("");
    const [hidePass, setHidePass] = useState(true);
    
    const [pass, setPass] = useState("");
   // const [hidePass, setHidePass] = useState(true);

    const [newpass, setNewpass] = useState("");
   // const [hidePass, setHidePass] = useState(true);
    
    
    return (
        <SafeAreaView style={styles.container}>
            <StatusBar />
            <Image source={logoWhite} style={styles.imageLogo} />
            <Animated.View style={styles.content}>
                <HeaderSection
                    title={"Senhas"}
                    leftIcon={true}
                    leftNavigation={"LoggedIn"}
                />
                <View style={styles.wrapForm}>
                    <Text style={styles.label}>Senha atual</Text>
                    <View style={styles.inputWrap}>
                        <TextInput
                            style={styles.inputForm}
                            keyboardType="default"
                            value={input}
                            onChangeText={(text) => setInput(text)}
                            secureTextEntry={hidePass}
                        />
                        <TouchableOpacity onPress={() => setHidePass(!hidePass)}>
                            {hidePass ? (
                                <Image source={eyeOff} style={styles.eyeOff} />
                            ) : (
                                <Image source={eye} style={styles.eye} />
                            )}
                        </TouchableOpacity>
                    </View>
                </View>

                <View style={styles.wrapForm}>
                    <Text style={styles.label}>Nova senha</Text>
                    <View style={styles.inputWrap}>
                        <TextInput
                            style={styles.inputForm}
                            keyboardType="default"
                            value={pass}
                            onChangeText={(text) => setPass(text)}
                            secureTextEntry={hidePass}
                        />
                        <TouchableOpacity onPress={() => setHidePass(!hidePass)}>
                            {hidePass ? (
                                <Image source={eyeOff} style={styles.eyeOff} />
                            ) : (
                                <Image source={eye} style={styles.eye} />
                            )}
                        </TouchableOpacity>
                    </View>
                </View>

                <View style={styles.wrapForm}>
                    <Text style={styles.label}>Repita a nova senha</Text>
                    <View style={styles.inputWrap}>
                        <TextInput
                            style={styles.inputForm}
                            keyboardType="default"
                            value={newpass}
                            onChangeText={(text) => setNewpass(text)}
                            secureTextEntry={hidePass}
                        />
                        <TouchableOpacity onPress={() => setHidePass(!hidePass)}>
                            {hidePass ? (
                                <Image source={eyeOff} style={styles.eyeOff} />
                            ) : (
                                <Image source={eye} style={styles.eye} />
                            )}
                        </TouchableOpacity>
                    </View>
                </View>

                <TouchableOpacity onPress={handleOk} style={styles.wrapButton}>
                    <View style={styles.wrapButton}>
                        <View style={styles.Button}>
                            <Text style={styles.ButtonText}>Alterar senha</Text>
                            <Ionicons name="chevron-forward" style={styles.ButtonIcon} />
                        </View>
                    </View>
                </TouchableOpacity>
            </Animated.View>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: "100%",
        alignItems: "center",
        backgroundColor: colors.dark_blue,
    },
    imageLogo: {
        width: 62,
        height: 23,
        marginTop: 38,
        marginBottom: 38,
    },
    content: {
        flex: 1,
        width: "100%",
        justifyContent: "flex-start",
        alignItems: "center",
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15,
        backgroundColor: colors.white,
        paddingTop: 40,
        // paddingHorizontal: 29,
    },
    header: {
        // flex: 1,
        flexDirection: "row",
        justifyContent: "space-between",
        paddingHorizontal: 29,
    },
    title: {
        fontSize: 21,
        fontFamily: fonts.regular,
        color: colors.dark_blue,
        textAlign: "left",
        paddingRight: 60,
        //marginRight:10
    },
    closeIcon: {
        fontSize: 36,
        color: colors.gray,
        marginTop: -6,
    },
    wrapForm: {
        marginHorizontal: 25,
        borderBottomWidth: 1,
        borderBottomColor: colors.light_gray,
        marginTop: 25
    },
    label: {
        fontSize: 16,
        fontFamily: fonts.regular,
        color: colors.dark_blue,

    },
    form: {
        justifyContent: "center",
        alignItems: "center",
    },
    inputForm: {
        width: "85%",
        color: colors.gray,
        fontSize: 30,
        //marginTop: 32,
        fontFamily: "Flexo-Regular",

        marginRight: 15,

    },
    inputWrap: {
        paddingVertical: 10,

        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
    },
    eyeOff: {
        width: 24,
        height: 19,
    },
    eye: {
        width: 25,
        height: 19,
    },
    wrapButton: {
        flex: 1,
        width: "100%",
        alignItems: "center",
        justifyContent: "flex-end",
    },
    Button: {
        padding: 18,
        paddingHorizontal: 29,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        width: "100%",
        backgroundColor: colors.light_blue,
    },
    ButtonText: {
        color: colors.white,
        fontSize: 22,
        fontFamily: fonts.regular,
    },
    ButtonIcon: {
        fontSize: 36,
        color: colors.white,
        marginTop: -1,
    },
});
