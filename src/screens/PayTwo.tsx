import { useNavigation } from "@react-navigation/core";
import React from "react";
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  View,
} from "react-native";
import { TextInput } from "react-native-gesture-handler";
import BoxBalance from "../components/BoxBalance";
import ConfirmButton from "../components/ConfirmButton";
import HeaderSection from "../components/HeaderSection";
import colors from "../styles/colors";
import fonts from "../styles/fonts";

export function PayTwo() {
  const navigation = useNavigation();
  return (
    <>
      <ScrollView>
        <SafeAreaView style={styles.container}>
          <StatusBar />
          <View style={styles.header}>
            <Text style={styles.userTitle}>Martin Ferramentas</Text>
          </View>
          <View style={styles.content}>
            <HeaderSection
              title="Digite o código de barras"
              leftIcon={true}
              leftNavigation={"PayOne"}
              rightIcon={false}
              rightNavigation={"LoggedIn"}
            />

            <View style={styles.wrapInner}>
              <BoxBalance balance={"24.895,36"} />
            </View>
            <View style={styles.wrapFooter}>
              <Text style={styles.sectionTitle}>Informe o código</Text>
              <View>
                <Text style={styles.label}>Boleto</Text>
                <TextInput
                  keyboardType="numeric"
                  style={styles.input}
                  placeholder="34191.79001 01043.510047 91020.150008 3 86190026000"
                />
              </View>
            </View>
          </View>
        </SafeAreaView>
      </ScrollView>

      <ConfirmButton title="Continuar" navTo={"PayThree"} />
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    alignItems: "center",
    backgroundColor: colors.dark_blue,
  },
  header: {
    paddingVertical: 34,
  },
  userTitle: {
    fontFamily: fonts.bold,
    color: colors.white,
    fontSize: 18,
    textAlign: "center",
  },
  content: {
    flex: 1,
    width: "100%",
    justifyContent: "flex-start",
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: colors.white,
    paddingTop: 40,
  },
  wrapInner: {
    width: "100%",
    paddingHorizontal: 29,
    paddingTop: 20,
  },

  sectionTitle: {
    fontFamily: fonts.regular,
    color: colors.light_blue,
    fontSize: 16,
    marginTop: 36,
  },
  wrapFooter: {
    justifyContent: "flex-end",
    paddingHorizontal: 29,
  },
  label: {
    fontFamily: fonts.regular,
    marginTop: 30,
  },
  input: {
    paddingTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: colors.light_gray,
    fontFamily: fonts.regular,
    fontSize: 14,
    color: colors.gray,
  },
});
