import { Ionicons } from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/core";
import React from "react";
import {
  Image,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import paidIcon from "../../assets/025-email.png";
import copy from "../../assets/copy2.png";
import qrcode from "../../assets/qrcode-generated.png";

import HeaderTransferSection from "../components/HeaderTransferSection";
import ConfirmButton from "../components/ConfirmButton";
import colors from "../styles/colors";
import fonts from "../styles/fonts";

export function ChargeReissueOpenLink() {
  return (
    <>
      <ScrollView>
        <SafeAreaView style={styles.container}>
          <StatusBar />

          <View style={styles.header}>
            <Text style={styles.userTitle}>Martin Ferramentas</Text>
          </View>
          <View style={styles.content}>
            <HeaderTransferSection
              sectionImage={paidIcon}
              title={"Sua cobrança foi gerada"}
              leftNavigation={"ChargeOpen"}
            />

            <View style={styles.wrapBoxes}>
              <View style={styles.boxQrCode}>
                <View style={styles.boxTile}></View>

                <View style={styles.wrapBoxQrCode}>
                  <View style={styles.boxqrCode}>
                    <Image source={qrcode} />
                  </View>
                  <Image style={styles.boxIcon} source={copy} />
                </View>
              </View>
            </View>
            <View style={styles.wrapBoxes}>
              <View style={styles.box}>
                <View style={styles.boxTile}></View>

                <View style={styles.wrapBoxCode}>
                  <Text style={styles.boxCode}>
                    34191.79001 01043.510047{"\n"}
                    91020.150008 3 86190026000
                  </Text>
                  <Image style={styles.boxIcon} source={copy} />
                </View>
              </View>
            </View>
            <View style={styles.wrapBoxes}>
              <View style={styles.box}>
                <View style={styles.boxTile}></View>

                <View style={styles.wrapBoxCode}>
                  <Text style={styles.boxCode}>https://stricba.nk/1Kcb1Tn</Text>
                  <Image style={styles.boxIcon} source={copy} />
                </View>
              </View>
            </View>
            <View style={styles.wrapFooter}>
              <View>
                <View style={styles.wrapDate}>
                  <Text style={styles.LabelText}>Vencimento</Text>
                  <Text style={styles.Text}>07/02/2022</Text>
                </View>
                <View style={styles.wrapValue}>
                  <Text style={styles.LabelText}>Valor do depósito</Text>
                  <Text style={styles.Text}>R$ 2.000</Text>
                </View>
              </View>
            </View>
          </View>
        </SafeAreaView>
      </ScrollView>
      <ConfirmButton title={"Compartilhar"} navTo={"LoggedIn"} />
    </>
  );
}

const styles = StyleSheet.create({
  ScrollView: {},
  container: {
    flex: 1,
    width: "100%",
    alignItems: "center",
    backgroundColor: colors.dark_blue,
  },
  header: {
    paddingVertical: 34,
  },
  userTitle: {
    fontFamily: fonts.bold,
    color: colors.white,
    fontSize: 18,
    textAlign: "center",
  },
  content: {
    flex: 1,
    width: "100%",

    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: colors.white,
    paddingTop: 25,
    paddingHorizontal: 29,
  },

  wrapBoxes: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    paddingVertical: 20,
  },
  boxQrCode: {
    width: "80%",
    borderRadius: 12,

    alignItems: "center",
    backgroundColor: colors.white,
    paddingBottom: 20,

    shadowColor: "#dcdcdc",
    borderBottomWidth: 0,
    shadowOffset: { width: 5, height: 9 },
    shadowOpacity: 0.1,
    shadowRadius: 2,
    elevation: 10,
  },
  boxTile: {
    width: 40,
    height: 4,
    backgroundColor: colors.orange,
  },
  wrapBoxQrCode: {
    flexDirection: "column",
    alignItems: "center",
  },
  boxqrCode: {
    paddingVertical: 25,
    paddingHorizontal: 30,
  },
  boxIcon: {
    width: 19,
    height: 19,
    resizeMode: "contain",
  },
  qrcode: {
    width: 180,
    height: 180,
    resizeMode: "contain",
  },
  //Box Codigos de barras
  box: {
    width: "100%",
    borderRadius: 12,

    borderColor: "#e3e3e3",

    alignItems: "center",
    backgroundColor: colors.white,

    shadowColor: "#dcdcdc",
    borderBottomWidth: 0,
    shadowOffset: { width: 5, height: 9 },
    shadowOpacity: 0.1,
    shadowRadius: 2,
    elevation: 10,
  },
  boxCode: {
    fontFamily: fonts.regular,
    color: colors.dark_blue,
    fontSize: 15,
    textAlign: "center",
    paddingVertical: 20,
    paddingHorizontal: 30,
  },
  wrapBoxCode: {
    paddingHorizontal: 38,

    flexDirection: "row",
    alignItems: "center",
  },

  wrapFooter: {
    flex: 1,
    paddingBottom: 40,
  },
  wrapDate: {
    marginTop: 38,
  },
  wrapValue: {
    marginTop: 24,
  },
  wrapdescription: {
    marginTop: 24,
  },
  LabelText: {
    fontFamily: fonts.regular,
    fontSize: 14,
    color: colors.dark_blue,
  },
  Text: {
    fontFamily: fonts.regular,
    fontSize: 16,
    color: colors.gray,
    paddingVertical: 4.5,
  },
  sectionFooter: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  warnLabel: {
    fontFamily: fonts.regular,
    color: colors.gray,
    fontSize: 14,
    textAlign: "center",
    marginTop: 66,
    marginBottom: 34,
  },

  wrapButton: {
    width: "100%",
    alignItems: "center",
    justifyContent: "flex-end",
  },
  Button: {
    padding: 18,
    flexDirection: "row",
    justifyContent: "space-between",
    width: "100%",
    alignItems: "center",
    backgroundColor: colors.light_blue,
  },
  ButtonText: {
    color: colors.white,
    fontSize: 22,
    fontFamily: fonts.regular,
  },
  ButtonIcon: {
    fontSize: 16,
    color: colors.white,
    marginTop: 0,
  },
});
