import { Ionicons } from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/core";
import React from "react";
import {
  Image,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Dimensions
} from "react-native";
import barcode from "../../assets/barcode.png";
import receive from "../../assets/receive.png";
import BoxBalance from "../components/BoxBalance";
import HeaderSection from "../components/HeaderSection";
import colors from "../styles/colors";
import fonts from "../styles/fonts";

export function DepositOne() {
  const navigation = useNavigation();

  function handleDepositTwo() {
    navigation.navigate("DepositTwo");
  }
  function handleDepositFour() {
    navigation.navigate("DepositFour");
  }
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar />
      <View style={styles.header}>
        <Text style={styles.userTitle}>Martin Ferramentas</Text>
      </View>
      <View style={styles.content}>
        <HeaderSection
          title="Depósitos"
          leftIcon={true}
          leftNavigation={"LoggedIn"}
        />
        <View style={styles.innerContent}>
          <BoxBalance balance="24.895,36" />
          <View style={styles.wrapBoxes}>
            <TouchableOpacity onPress={handleDepositTwo} style={styles.box}>
              <View style={styles.boxTile}></View>
              <Image style={styles.boxIcon} source={barcode} />
              <Text style={styles.boxTitle}>
                Depósito{"\n"}
                por Boleto
              </Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={handleDepositFour} style={styles.box}>
              <View style={styles.boxTile}></View>
              <Image style={styles.boxIcon} source={receive} />
              <Text style={styles.boxTitle}>
                Depósito{"\n"}
                por Transfer.
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.wrapFooter}>
          <View style={styles.sectionFooter}>
            <TouchableOpacity>
              <Text style={styles.settingsLink}>Configurações</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.sectionFooter}>
            <TouchableOpacity>
              <Text style={styles.historyButton}>Histórico de depósitos</Text>
            </TouchableOpacity>
            <Ionicons name="chevron-forward" style={styles.arrow} />
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    alignItems: "center",
    backgroundColor: colors.dark_blue,
  },
  header: {
    paddingVertical: 34,
  },
  userTitle: {
    fontFamily: fonts.bold,
    color: colors.white,
    fontSize: 18,
    textAlign: "center",
  },
  headerContent: {
    //flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  content: {
    flex: 1,
    width: "100%",
    justifyContent: "flex-start",
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: colors.white,
    paddingTop: 40,
    // paddingHorizontal: 29,
  },
  innerContent: {
    paddingHorizontal: 29,
    paddingTop: 20,
  },
  titleContent: {
    fontSize: 21,
    fontFamily: fonts.regular,
    color: colors.dark_blue,
    textAlign: "left",
    paddingRight: 180,
  },
  backIcon: {
    fontSize: 36,
    color: colors.light_blue,
  },
  cancelButton: {
    fontFamily: fonts.regular,
    color: colors.light_blue,
    fontSize: 16,
  },
  wrapBalance: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    borderWidth: 1,
    borderColor: colors.light_gray,
    borderRadius: 10,
    paddingHorizontal: 20,
    paddingVertical: 15,
  },
  balanceTitle: {
    fontFamily: fonts.regular,
    fontSize: 18,
    color: colors.dark_blue,
  },
  balanceValue: {
    fontFamily: fonts.bold,
    fontSize: 18,
    color: colors.dark_blue,
  },
  icon: {
    width: 24,
    height: 18,
    resizeMode: "contain",
  },
  wrapBoxes: {
    flexDirection: "row",
    alignItems: "flex-start",
    paddingVertical: 20,
  },
  box: {
    width: Dimensions.get("screen").width / 4 + 10,
    //width: 117,
    height: 128,
    //borderWidth: 1,
    borderRadius: 12,
    borderColor: "#e3e3e3",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: colors.white,
    paddingHorizontal: 13,
    paddingBottom: 13,
    marginRight: 15,

     // borderWidth: 0,
    // borderColor: colors.light_gray,
   // backgroundColor: "#fff",
    shadowColor: "#a9a9a9",
    borderBottomWidth: 0,
    shadowOffset: { width: 5, height: 9 },
    shadowOpacity: 0.1,
    shadowRadius: 2,
    elevation: 10,
  },
  boxTile: {
    width: 40,
    height: 4,
    backgroundColor: colors.light_blue,
  },
  boxIcon: {
    width: 30,
    height: 30,
    resizeMode: "contain",
  },
  boxTitle: {
    fontFamily: fonts.regular,
    color: colors.dark_blue,
    fontSize: 15,
  },
  wrapFooter: {
    flex: 1,
    justifyContent: "flex-end",
    paddingHorizontal: 29,
  },
  sectionFooter: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingVertical: 28,
    alignItems: "center",
  },
  settingsLink: {
    fontFamily: fonts.regular,
    color: colors.light_blue,
    fontSize: 16,
  },
  historyButton: {
    fontFamily: fonts.regular,
    color: colors.dark_blue,
    fontSize: 16,
  },
  arrow: {
    fontSize: 24,
    color: colors.light_blue,
  },
});
