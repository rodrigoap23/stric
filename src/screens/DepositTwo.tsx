import { Ionicons } from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/core";
import React, { useState } from "react";
import {
  Image,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { TextInputMask } from "react-native-masked-text";
import eye from "../../assets/eye.png";
import HeaderSection from "../components/HeaderSection";
import colors from "../styles/colors";
import fonts from "../styles/fonts";

export function DepositTwo() {
  const [money, setMoney] = useState("0,00");
  const navigation = useNavigation();

  function handleClose() {
    navigation.navigate("Lobby");
  }
  function handleBack() {
    navigation.navigate("Lobby");
  }
  function handleNext() {
    navigation.navigate("DepositThree");
  }

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar />
      <View style={styles.header}>
        <Text style={styles.userTitle}>Martin Ferramentas</Text>
      </View>
      <View style={styles.content}>
        <HeaderSection
          title="Gerar Boleto"
          leftIcon={true}
          leftNavigation={"DepositOne"}
        />
        <View style={styles.wrapInner}>
          <View style={styles.wrapBalance}>
            <Text style={styles.balanceTitle}>Saldo:</Text>
            <Text style={styles.balanceValue}>R$ 24.895,36</Text>
            <TouchableOpacity>
              <Image style={styles.icon} source={eye} />
            </TouchableOpacity>
          </View>
          <View style={styles.wrapDepositValue}>
            <Text style={styles.depositFormLabel}>Valor do boleto:</Text>
            <View>
              <TextInputMask
                style={styles.inputValue}
                type="money"
                options={{
                  precision: 2,
                  separator: ",",
                  delimiter: ".",
                  unit: "R$ ",
                }}
                value={money}
                onChangeText={(text) => setMoney(text)}
              />
            </View>
          </View>
        </View>
        <View style={styles.wrapFooter}>
          <View style={styles.sectionFooter}>
            <Text style={styles.warnLabel}>
              Você será cobrado em R$ 4,99 por boleto gerado
            </Text>
          </View>
        </View>
      </View>
      <TouchableOpacity onPress={handleNext} style={styles.wrapButton}>
        <View style={styles.wrapButton}>
          <View style={styles.Button}>
            <Text style={styles.ButtonText}>Emitir boleto</Text>
            <Ionicons name="chevron-forward" style={styles.ButtonIcon} />
          </View>
        </View>
      </TouchableOpacity>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    alignItems: "center",
    backgroundColor: colors.dark_blue,
  },
  header: {
    paddingVertical: 34,
  },
  userTitle: {
    fontFamily: fonts.bold,
    color: colors.white,
    fontSize: 18,
    textAlign: "center",
  },
  headerContent: {
    //flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  content: {
    flex: 1,
    width: "100%",
    justifyContent: "flex-start",
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: colors.white,
    paddingTop: 40,
  },
  wrapInner: {
    paddingHorizontal: 29,
    paddingTop: 20,
  },
  titleContent: {
    fontSize: 21,
    fontFamily: fonts.regular,
    color: colors.dark_blue,
    textAlign: "left",
    paddingRight: 180,
  },
  backIcon: {
    fontSize: 36,
    color: colors.light_blue,
  },
  cancelButton: {
    fontFamily: fonts.regular,
    color: colors.light_blue,
    fontSize: 16,
  },
  wrapBalance: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    borderWidth: 1,
    borderColor: colors.light_gray,
    borderRadius: 10,
    paddingHorizontal: 20,
    paddingVertical: 15,
  },
  balanceTitle: {
    fontFamily: fonts.regular,
    fontSize: 18,
    color: colors.dark_blue,
  },
  balanceValue: {
    fontFamily: fonts.bold,
    fontSize: 18,
    color: colors.dark_blue,
  },
  icon: {
    width: 24,
    height: 18,
    resizeMode: "contain",
  },
  wrapDepositValue: {
    alignItems: "center",
    paddingVertical: 20,
  },
  depositFormLabel: {
    fontFamily: fonts.regular,
    fontSize: 18,
    color: colors.dark_blue,
    marginBottom: 10,
  },
  inputValue: {
    fontFamily: fonts.regular,
    fontSize: 32,
    color: colors.light_blue,
    borderBottomWidth: 1,
    borderBottomColor: colors.light_gray,
    paddingBottom: 10,
  },
  wrapFooter: {
    flex: 1,
    justifyContent: "flex-end",
    alignItems: "center",
  },
  sectionFooter: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingVertical: 34,
    alignItems: "center",
  },
  warnLabel: {
    fontFamily: fonts.regular,
    color: colors.gray,
    fontSize: 14,
  },
  wrapButton: {
    width: "100%",
    alignItems: "center",
    justifyContent: "flex-end",
  },
  Button: {
    padding: 18,
    flexDirection: "row",
    justifyContent: "space-between",
    width: "100%",
    alignItems: "center",
    backgroundColor: colors.light_blue,
  },
  ButtonText: {
    color: colors.white,
    fontSize: 22,
    fontFamily: fonts.regular,
  },
  ButtonIcon: {
    fontSize: 16,
    color: colors.white,
    marginTop: 0,
  },
});
