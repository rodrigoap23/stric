import { useNavigation } from "@react-navigation/core";
import React from "react";
import {
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import music from "../../assets/music.png";
import ContactItem from "../components/ContactItem";
import HeaderSection from "../components/HeaderSection";
import colors from "../styles/colors";
import fonts from "../styles/fonts";

export function TransfContact() {
  const navigation = useNavigation();

  function handleTedTwo() {
    navigation.navigate("TransfTedTwo");
  }
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar />
      <View style={styles.header}>
        <Text style={styles.userTitle}>Martin Ferramentas</Text>
      </View>
      <View style={styles.content}>
        <HeaderSection
          title={"Contatos"}
          leftIcon={true}
          leftNavigation={"TransferTedOne"}
          rightIcon={false}
          rightNavigation={"LoggeddIn"}
        />
        <View style={styles.innerContent}>
          <View style={styles.wrapList}>
            <TouchableOpacity onPress={handleTedTwo}>
              <ContactItem
                name={"Adalberto Brito"}
                initials={"AB"}
                color={colors.dark_blue}
              />
            </TouchableOpacity>
            <ContactItem
              name={"Lucas Santos"}
              initials={"LS"}
              color={colors.dark_blue}
            />
            <ContactItem
              icon={music}
              name={"Tiago Luiz"}
              initials={"TL"}
              color={colors.dark_blue}
              favorite={true}
            />
            <ContactItem
              name={"Alex Figueiredo"}
              initials={"AF"}
              color={colors.dark_blue}
            />
            <ContactItem
              name={"Patricia Santanna"}
              initials={"PS"}
              color={colors.dark_blue}
            />
            <ContactItem
              icon={music}
              title={"Transferência via Pix"}
              name={"Fernanda Fernandes"}
              initials={"FF"}
              color={colors.dark_blue}
            />
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    alignItems: "center",
    backgroundColor: colors.dark_blue,
    justifyContent: "center",
  },
  header: {
    paddingVertical: 34,
  },
  userTitle: {
    fontFamily: fonts.bold,
    color: colors.white,
    fontSize: 18,
    textAlign: "center",
  },
  content: {
    flex: 1,
    width: "100%",
    justifyContent: "flex-start",
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: colors.white,
    paddingTop: 40,
  },
  innerContent: {
    paddingHorizontal: 29,
    paddingTop: 20,
  },
  wrapBoxes: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingVertical: 20,
  },
  titleSection: {
    fontFamily: fonts.medium,
    color: colors.dark_blue,
    fontSize: 14,
    width: "100%",
    textAlign: "center",
    paddingVertical: 20,
  },
  wrapList: {},
});
