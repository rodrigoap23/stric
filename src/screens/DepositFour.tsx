import { Ionicons } from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/core";
import React from "react";
import {
  Image,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import paidIcon from "../../assets/025email.png";
import copy from "../../assets/copy.png";
import HeaderTransferSection from "../components/HeaderTransferSection";
import colors from "../styles/colors";
import fonts from "../styles/fonts";

export function DepositFour() {
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar />
      <View style={styles.header}>
        <Text style={styles.userTitle}>Martin Ferramentas</Text>
      </View>
      <View style={styles.content}>
        <HeaderTransferSection
          sectionImage={paidIcon}
          title={"Depósito por transferência"}
          leftNavigation={"DepositOne"}
        />
        <View style={styles.wrapSubtitle}>
          <Text style={styles.subTitleText}>
            Use os dados abaixo para{"\n"}
            transferir para sua conta Stric.
          </Text>
        </View>
        <View style={styles.wrapFooter}>
          <View style={styles.footerInner}>
            <View>
              <Text style={styles.title}>Banco</Text>
              <Text style={styles.subTitle}>123</Text>
            </View>
            <TouchableOpacity>
              <Image style={styles.copyIcon} source={copy} />
            </TouchableOpacity>
          </View>
          <View style={styles.footerInner}>
            <View>
              <Text style={styles.title}>Agência</Text>
              <Text style={styles.subTitle}>0001</Text>
            </View>
            <TouchableOpacity>
              <Image style={styles.copyIcon} source={copy} />
            </TouchableOpacity>
          </View>
          <View style={styles.footerInner}>
            <View>
              <Text style={styles.title}>Conta Corrente</Text>
              <Text style={styles.subTitle}>131231928-2</Text>
            </View>
            <TouchableOpacity>
              <Image style={styles.copyIcon} source={copy} />
            </TouchableOpacity>
          </View>
          <View style={styles.sectionFooter}>
            <Text style={styles.warnLabel}>Essa transação é gratuita</Text>
          </View>
        </View>
      </View>
      <TouchableOpacity onPress={() => {}} style={styles.wrapButton}>
        <View style={styles.wrapButton}>
          <View style={styles.Button}>
            <Text style={styles.ButtonText}>Compartilhar</Text>
            <Ionicons name="chevron-forward" style={styles.ButtonIcon} />
          </View>
        </View>
      </TouchableOpacity>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    alignItems: "center",
    backgroundColor: colors.dark_blue,
  },
  header: {
    paddingVertical: 34,
  },
  userTitle: {
    fontFamily: fonts.bold,
    color: colors.white,
    fontSize: 18,
    textAlign: "center",
  },
  content: {
    flex: 1,
    width: "100%",

    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: colors.white,
    paddingTop: 25,
    paddingHorizontal: 29,
  },
  wrapSubtitle: {
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "center",
    paddingVertical: 25,
  },
  subTitleText: {
    fontFamily: fonts.regular,
    color: colors.dark_blue,
    fontSize: 16,
    textAlign: "center",
  },

  wrapFooter: {
    flex: 1,
    marginTop: 21,
  },
  footerInner: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginTop: 25,
  },
  title: {
    fontFamily: fonts.regular,
    color: colors.dark_blue,
    fontSize: 14,
  },
  subTitle: {
    fontFamily: fonts.regular,
    color: colors.gray,
    fontSize: 16,
    marginTop: 5,
  },
  copyIcon: {
    width: 20,
    height: 20,
    resizeMode: "contain",
  },
  sectionFooter: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  warnLabel: {
    fontFamily: fonts.regular,
    color: colors.gray,
    fontSize: 14,
    textAlign: "center",
    marginTop: 66,
    marginBottom: 34,
  },

  wrapButton: {
    width: "100%",
    alignItems: "center",
    justifyContent: "flex-end",
  },
  Button: {
    padding: 18,
    flexDirection: "row",
    justifyContent: "space-between",
    width: "100%",
    alignItems: "center",
    backgroundColor: colors.light_blue,
  },
  ButtonText: {
    color: colors.white,
    fontSize: 22,
    fontFamily: fonts.regular,
  },
  ButtonIcon: {
    fontSize: 16,
    color: colors.white,
    marginTop: 0,
  },
});
