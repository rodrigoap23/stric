import { Entypo } from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/core";
import React from "react";
import {
  Dimensions,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { color } from "react-native-reanimated";
import logo from "../../assets/logo.png";
import colors from "../styles/colors";
import fonts from "../styles/fonts";

export function Lobby() {
  const navigation = useNavigation();

  function handleLogin() {
    navigation.navigate("LoginAccountOne");
  }
  function handleCreateAccount() {
    navigation.navigate("CreateAccountOne");
  }
  function handleForgotenAccount() {
    navigation.navigate("ForgotenAccountOne");
  }

  return (
    <ScrollView>
      <View style={styles.container}>
        <View style={styles.wrapLogo}>
          <Image style={styles.logo} resizeMode="contain" source={logo} />
        </View>
        <View style={styles.wrapContent}>
          <TouchableOpacity onPress={handleLogin}>
            <View style={styles.DarkBox}>
              <View style={styles.wrapNotchClear}>
                <View style={styles.notchClear}></View>
              </View>
              <Text style={styles.titleDarkBox}>Minha Conta Digital PJ</Text>
              <View style={styles.wrapButtonDarkBox}>
                <Text style={styles.buttonDarkBox}>Acessar</Text>
                <Text>
                  <Entypo
                    style={styles.DarkBoxButtonIcon}
                    name="chevron-thin-right"
                  />
                </Text>
              </View>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={styles.ClearBox} onPress={handleCreateAccount}>
              <View style={styles.wrapNotchDark}>
                <View style={styles.notchDark}></View>
              </View>
              <Text style={styles.titleClearBox}>Abrir minha conta PJ</Text>
              <Text style={styles.subTitleClearBox}>
                Sua empresa livre de burocracias e de taxas.
              </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.wrapFooter}>
          <TouchableOpacity onPress={handleForgotenAccount}>
            <Text style={styles.forgotPassButton}>
              Esqueci minha senha
              <Entypo name="chevron-right" />
            </Text>
          </TouchableOpacity>
          <Text style={styles.warnText}>
            Em caso de dúvidas entre em contato{"\n"}
            Capitais e região metropolitana: 1234-1234{"\n"}
            Demais localidades: 0800 123 1234
          </Text>
        </View>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "space-around",
  },
  wrapLogo: {
    marginTop: 126,
  },
  logo: {
    width: Dimensions.get("window").width * 0.7,
    height: 47,
  },
  wrapContent: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  DarkBox: {
    // alignItems: "center",
    paddingLeft: 20,
    paddingRight: 18,
    paddingBottom: 20,
    justifyContent: "flex-end",
    backgroundColor: colors.dark_blue,
    borderRadius: 20,
    width: 180,
    height: 392,
    marginTop: 91,
    marginRight: 15,
  },
  wrapNotchClear: {
    flex: 1,
    alignItems: "center",
  },
  notchClear: {
    borderRadius: 3,
    backgroundColor: colors.light_blue,
    width: 39,
    height: 6,
  },
  wrapButtonDarkBox: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 15,
  },
  buttonDarkBox: {
    color: "#fff",
    fontSize: 12,
    justifyContent: "flex-start",
    fontFamily: fonts.regular,
  },
  DarkBoxButtonIcon: {
    color: colors.light_blue,
  },
  titleDarkBox: {
    color: "#fff",
    fontFamily: "Flexo-Bold",
    fontSize: 22,
  },
  ClearBox: {
    width: 180,
    height: 392,
    borderRadius: 20,
    marginTop: 118,
    paddingLeft: 20,
    paddingRight: 18,
    paddingBottom: 20,
    marginLeft: 5,
     //borderWidth: 1,
    borderColor: colors.light_gray,

    // borderWidth: 0,
    // borderColor: colors.light_gray,
    backgroundColor: "#fff",
    shadowColor: "#707070",
    borderBottomWidth: 0,
    shadowOffset: { width: 5, height: 9 },
    shadowOpacity: 0.1,
    shadowRadius: 2,
    elevation: 10,
  },
  wrapNotchDark: {
    flex: 1,
    alignItems: "center",
  },
  notchDark: {
    borderRadius: 3,
    backgroundColor: colors.dark_blue,
    width: 39,
    height: 6,
  },
  titleClearBox: {
    color: colors.light_blue,
    fontFamily: fonts.bold,
    fontSize: 22,
  },
  subTitleClearBox: {
    textAlign: "left",
    color: colors.dark_blue,
    fontFamily: fonts.regular,
    fontSize: 12,
    marginTop: 13,
  },
  wrapFooter: {
    paddingTop: 61,
  },
  forgotPassButton: {
    // paddingTop: 61,
    textAlign: "center",
    fontFamily: fonts.regular,
    fontSize: 12,
    color: colors.gray,
  },
  warnText: {
    paddingTop: 41,
    paddingBottom: 60,
    textAlign: "center",
    fontFamily: fonts.regular,
    color: colors.gray,
    fontSize: 12,
  },
});
