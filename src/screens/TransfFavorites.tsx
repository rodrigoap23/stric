import React from "react";
import { SafeAreaView, StatusBar, StyleSheet, Text, View } from "react-native";
import ContactItem from "../components/ContactItem";
import HeaderSection from "../components/HeaderSection";
import colors from "../styles/colors";
import fonts from "../styles/fonts";

export function TransfFavorites() {
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar />
      <View style={styles.header}>
        <Text style={styles.userTitle}>Martin Ferramentas</Text>
      </View>
      <View style={styles.content}>
        <HeaderSection
          title={"Favoritos"}
          leftIcon={true}
          leftNavigation={"TransferTedOne"}
        />
        <View style={styles.innerContent}>
          <ContactItem
            initials={"AB"}
            name={"Adalberto Brito"}
            color={colors.dark_blue}
            favorite={true}
          />
          <ContactItem
            initials={"LS"}
            name={"Lucas Santos"}
            color={colors.dark_blue}
            favorite={true}
          />
          <ContactItem
            initials={"TL"}
            name={"Thiago Luiz"}
            color={colors.dark_blue}
            favorite={true}
          />
          <ContactItem
            initials={"AF"}
            name={"Alex Figueiredo"}
            color={colors.dark_blue}
            favorite={true}
          />
          <ContactItem
            initials={"PS"}
            name={"Patricia Santanna"}
            color={colors.dark_blue}
            favorite={true}
          />
          <ContactItem
            initials={"FF"}
            name={"Fernanda Fernandes"}
            color={colors.dark_blue}
            favorite={true}
          />
        </View>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    alignItems: "center",
    backgroundColor: colors.dark_blue,
    justifyContent: "center",
  },
  header: {
    paddingVertical: 34,
  },
  userTitle: {
    fontFamily: fonts.bold,
    color: colors.white,
    fontSize: 18,
    textAlign: "center",
  },
  content: {
    flex: 1,
    width: "100%",
    justifyContent: "flex-start",
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: colors.white,
    paddingTop: 40,
  },
  innerContent: {
    paddingHorizontal: 29,
    paddingTop: 20,
  },
  wrapBoxes: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingVertical: 20,
  },
  titleSection: {
    fontFamily: fonts.medium,
    color: colors.dark_blue,
    fontSize: 14,
    width: "100%",
    textAlign: "center",
    paddingVertical: 20,
  },
});
