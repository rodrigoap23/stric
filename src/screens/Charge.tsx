import { useNavigation } from "@react-navigation/core";
import React from "react";
import { SafeAreaView, StatusBar, StyleSheet, Text, View, TouchableOpacity } from "react-native";
import { Ionicons } from "@expo/vector-icons";

import barcodecharge from "../../assets/barcode-charge.png";
import cardcharge from "../../assets/cardcharge.png";
import qrcodecharge from "../../assets/qrcodecharge.png";
import link from "../../assets/link.png";


import BoxBalance from "../components/BoxBalance";
import BoxItem from "../components/BoxItem";
import HeaderSection from "../components/HeaderSection";
import colors from "../styles/colors";
import fonts from "../styles/fonts";
import { ScrollView } from "react-native-gesture-handler";

export function Charge() {
  const navigation = useNavigation();

  return (
    <>
      <ScrollView>
        <SafeAreaView style={styles.container}>
          <StatusBar />
          <View style={styles.header}>
            <Text style={styles.userTitle}>Martin Ferramentas</Text>
          </View>
          <View style={styles.content}>
            <HeaderSection
              title="Cobrar"
              leftIcon={true}
              leftNavigation={"LoggedIn"}
              rightIcon={false}
              rightNavigation={"LoggedIn"}
            />
            <View style={styles.innerContent}>
              <BoxBalance balance="24.895,36" />
              <View style={styles.wrapBoxes}>
                <BoxItem
                  useImageIcon={true}
                  imageIcon={barcodecharge}
                  title="Cobrança por boleto"
                  navTo={"ChargePaySlip"}
                />
                <BoxItem
                  useImageIcon={true}
                  imageIcon={cardcharge}
                  title="Cobrança link cartão"
                  navTo={"ChargeLinkCard"}
                />
                <BoxItem
                  useImageIcon={true}
                  imageIcon={qrcodecharge}
                  title="Cobrança por Pix"
                  navTo={"ChargePix"}
                />
              </View>
              <View style={styles.wrapBoxes}>
                <BoxItem
                  useImageIcon={true}
                  imageIcon={link}
                  title="Cobrança link aberto"
                  navTo={"ChargeOpenLink"}
                />
              </View>
            </View>
            <View style={styles.wrapFooter}>


              <TouchableOpacity onPress={()=>{navigation.navigate("ChargeOpen")}}>
                <View
                  style={[
                    styles.sectionFooter,
                    { borderTopWidth: 1, borderTopColor: colors.light_gray },
                  ]}
                >
                  <Text style={styles.historyButton}>Em aberto</Text>
                  <View style={styles.wrapNotify}>
                    <View style={styles.badgeNotification}>
                      <Text style={styles.badgeInner}>4</Text>
                    </View>
                  </View>
                  <Ionicons name="chevron-forward" style={styles.arrow} />
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate("RecentStatement");
                }}
              >
                <View
                  style={[
                    styles.sectionFooter,
                    { borderTopWidth: 1, borderTopColor: colors.light_gray },
                  ]}
                >
                  <Text style={styles.historyButton}>Extrato</Text>
                  <View style={styles.wrapNotify}>
                    {/* <Ionicons name="star" style={styles.iconStar} /> */}
                  </View>
                  <Ionicons name="chevron-forward" style={styles.arrow} />
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => { navigation.navigate("SearchCharge") }}>
                <View
                  style={[
                    styles.sectionFooter,
                    { borderTopWidth: 1, borderTopColor: colors.light_gray },
                  ]}
                >
                  <Text style={styles.historyButton}>Buscar cobrança</Text>
                  <Ionicons name="chevron-forward" style={styles.arrow} />
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => { navigation.navigate("Manage") }}>
                <View
                  style={[
                    styles.sectionFooter,
                    { borderTopWidth: 1, borderTopColor: colors.light_gray },
                  ]}
                >
                  <Text style={styles.historyButton}>Gráficos de projeção de recebimentos</Text>
                  <Ionicons name="chevron-forward" style={styles.arrow} />
                </View>
              </TouchableOpacity>

            </View>
          </View>
        </SafeAreaView>
      </ScrollView>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    alignItems: "center",
    backgroundColor: colors.dark_blue,
  },
  header: {
    paddingVertical: 34,
  },
  userTitle: {
    fontFamily: fonts.bold,
    color: colors.white,
    fontSize: 18,
    textAlign: "center",
  },
  content: {
    flex: 1,
    width: "100%",
    justifyContent: "flex-start",
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: colors.white,
    paddingTop: 40,
    // paddingHorizontal: 29,
  },
  innerContent: {
    paddingHorizontal: 29,
    paddingTop: 20,
  },

  wrapBoxes: {
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "space-between",
    paddingTop: 20,
  },

  wrapFooter: {
    flex: 1,
    justifyContent: "flex-start",
    paddingHorizontal: 29,
    marginTop: 40,

  },
  sectionFooter: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingVertical: 26,
    alignItems: "center",
  },
  sectionTitle: {
    fontFamily: fonts.regular,
    color: colors.light_blue,
    fontSize: 16,
  },
  historyButton: {
    fontFamily: fonts.regular,
    color: colors.dark_blue,
    fontSize: 16,
  },
  arrow: {
    fontSize: 24,
    color: colors.light_blue,
  },
  wrapNotify: {
    flex: 1,
    justifyContent: "flex-end",
    alignItems: "flex-end",
    paddingRight: 10,
  },
  iconStar: {
    fontSize: 24,
    color: colors.orange,
    justifyContent: "flex-end",
  },
  badgeNotification: {
    backgroundColor: colors.red,
    paddingHorizontal: 7,
    borderRadius: 5,
  },
  badgeInner: {
    fontFamily: fonts.bold,
    color: colors.white,
  },
});
