import { useNavigation } from "@react-navigation/core";
import React, { useState } from "react";
import {
  Animated,
  Image,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  View,
} from "react-native";
import { TextInputMask } from "react-native-masked-text";
import logoWhite from "../../assets/logoWhite.png";
import ConfirmButton from "../components/ConfirmButton";
import HeaderSection from "../components/HeaderSection";
import colors from "../styles/colors";
import fonts from "../styles/fonts";

export function CreateAccountOne() {
  const [cell, setCell] = useState("");
  const navigation = useNavigation();

  function handleClose() {
    navigation.navigate("Lobby");
  }
  function handleNext() {
    navigation.navigate("CreateAccountTwo");
  }
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar />
      <Image source={logoWhite} style={styles.imageLogo} />
      <Animated.View style={styles.content}>
        <HeaderSection
          title={"Insira um telefone válido"}
          rightIcon={true}
          rightNavigation={"Lobby"}
        />
        <TextInputMask
          style={styles.inputForm}
          type="cel-phone"
          options={{
            maskType: "BRL",
            withDDD: true,
            dddMask: " (99) ",
          }}
          value={cell}
          onChangeText={(text) => setCell(text)}
        />
        <View style={styles.Footer}>
          <ConfirmButton title={"Conttinuar"} navTo={"CreateAccountTwo"} />
        </View>
      </Animated.View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    alignItems: "center",
    backgroundColor: colors.dark_blue,
  },
  imageLogo: {
    width: 62,
    height: 23,
    marginTop: 38,
    marginBottom: 38,
  },
  content: {
    flex: 1,
    width: "100%",
    justifyContent: "flex-start",
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: colors.white,
    paddingTop: 40,
  },
  header: {
    // flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: 29,
  },
  title: {
    fontSize: 21,
    fontFamily: fonts.regular,
    color: colors.dark_blue,
  },
  closeIcon: {
    fontSize: 36,
    color: colors.light_blue,
    marginTop: -6,
  },
  form: {
    justifyContent: "center",
    alignItems: "center",
  },
  inputForm: {
    width: "100%",
    color: colors.light_blue,
    fontSize: 30,
    marginTop: 32,
    marginHorizontal: 29,
    fontFamily: fonts.regular,
    borderLeftWidth: 1,
    borderLeftColor: colors.light_gray,
  },
  Footer: {
    backgroundColor: colors.white,
    flex: 1,
    justifyContent: "flex-end",
    width: "100%",
  },
  // wrapButton: {
  //     flex: 1,
  //     width: "100%",
  //     alignItems: "center",
  //     justifyContent: "flex-end",
  // },
  // Button: {
  //     padding: 18,
  //     flexDirection: "row",
  //     justifyContent: "space-between",
  //     width: "100%",
  //     backgroundColor: colors.light_blue,
  // },
  // ButtonText: {
  //     color: colors.white,
  //     fontSize: 22,
  //     fontFamily: fonts.regular,
  // },
  // ButtonIcon: {
  //     fontSize: 36,
  //     color: colors.white,
  //     marginTop: -9,

  // },
});
