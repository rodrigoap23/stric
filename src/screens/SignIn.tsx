import { useNavigation } from "@react-navigation/core";
import React from "react";
import {
  Image,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  View,
} from "react-native";
import star from "../../assets/staroutline.png";
import ConfirmButton from "../components/ConfirmButton";
import HeaderSection from "../components/HeaderSection";
import colors from "../styles/colors";
import fonts from "../styles/fonts";

export function SignIn() {
  const navigation = useNavigation();

  function handleClose() {
    navigation.navigate("Lobby");
  }
  function handleBack() {
    navigation.navigate("LoggedIn");
  }
  function handleDepositTwo() {
    // navigation.navigate('DepositTwo');
  }
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar />
      <View style={styles.header}>
        <Text style={styles.userTitle}>Martin Ferramentas</Text>
      </View>
      <View style={styles.content}>
        <HeaderSection
          title={"Assinatura"}
          leftIcon={true}
          leftNavigation={"LoggedIn"}
        />
        <View style={styles.wrapContent}>
          <Image style={styles.star} source={star} />
          <Text style={styles.title}>Torne-se premium!</Text>
          <Text style={styles.subtitle}>Veja os benefícios:</Text>
          <Text style={styles.text}>
            Lorem ipsum dolor aliqua enim;{"\n\n"}
            Sit amet, consectetur adipiscing;{"\n\n"}
            Sed do eiusmod tempor Incididunt ut;{"\n\n"}
          </Text>
          <Text style={styles.subtitle}>E também:</Text>
          <Text style={styles.text}>
            Lorem ipsum dolor aliqua enim;{"\n\n"}
            Sit amet, consectetur adipiscing;{"\n\n"}
            Sed do eiusmod tempor Incididunt ut;{"\n\n"}
          </Text>
        </View>
      </View>
      <ConfirmButton title={"Tornar-se Premium"} navTo={"LoggedIn"} />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    alignItems: "center",
    backgroundColor: colors.dark_blue,
    justifyContent: "center",
  },
  header: {
    paddingVertical: 34,
  },
  userTitle: {
    fontFamily: fonts.bold,
    color: colors.white,
    fontSize: 18,
    textAlign: "center",
  },

  content: {
    flex: 1,
    width: "100%",
    justifyContent: "flex-start",
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: colors.white,
    paddingTop: 40,
  },
  wrapContent: {
    justifyContent: "center",
    alignItems: "center",
  },
  star: {
    width: 82,
    height: 82,
    marginTop: 70,
  },
  title: {
    fontFamily: fonts.bold,
    color: colors.dark_blue,
    fontSize: 24,
    marginTop: 48,
    marginBottom: 48,
    textAlign: "center",
  },
  subtitle: {
    fontFamily: fonts.regular,
    color: colors.dark_blue,
    fontSize: 16,
    marginVertical: 17,
  },
  text: {
    fontFamily: fonts.regular,
    color: colors.gray,
    fontSize: 14,
    textAlign: "center",
  },
});
