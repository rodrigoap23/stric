import { Ionicons } from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/core";
import React from "react";
import {
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import HeaderSection from "../components/HeaderSection";
import colors from "../styles/colors";
import fonts from "../styles/fonts";

export function Account() {
  const navigation = useNavigation();

  function handleClose() {
    navigation.navigate("Lobby");
  }
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar />
      <View style={styles.header}>
        <Text style={styles.userTitle}>Martin Ferramentas</Text>
      </View>
      <View style={styles.content}>
        <HeaderSection title={"Conta"} />
        <View style={styles.wrapContent}>
          <Text style={styles.userName}>Fernando Sampaio Almeida</Text>
          <Text style={styles.userAgency}>
            Agência 0001 Conta 23345192-5{"\n\n"}
            Banco 123 - Stric Pagamentos S.A.
          </Text>
        </View>
        <View style={styles.wrapFooter}>
          <TouchableOpacity onPress={()=>{navigation.navigate("MyAccount")}}>
            <View style={styles.sectionFooter}>
              <Text style={styles.historyButton}>Minhas conta</Text>
              <Ionicons name="chevron-forward" style={styles.arrow} />
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={()=>{navigation.navigate("PasswordChange")}}>
            <View
              style={[
                styles.sectionFooter,
                { borderTopWidth: 1, borderTopColor: colors.light_gray },
              ]}
            >
              <Text style={styles.historyButton}>Senhas</Text>
              <Ionicons name="chevron-forward" style={styles.arrow} />
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate("SignIn");
            }}
          >
            <View
              style={[
                styles.sectionFooter,
                { borderTopWidth: 1, borderTopColor: colors.light_gray },
              ]}
            >
              <Text style={styles.historyButton}>Assinatura</Text>
              <View style={styles.wrapNotify}>
                <Ionicons name="star" style={styles.iconStar} />
              </View>
              <Ionicons name="chevron-forward" style={styles.arrow} />
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={()=>{navigation.navigate("Prices")}}>
            <View
              style={[
                styles.sectionFooter,
                { borderTopWidth: 1, borderTopColor: colors.light_gray },
              ]}
            >
              <Text style={styles.historyButton}>Tarifas e Limites</Text>
              <Ionicons name="chevron-forward" style={styles.arrow} />
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={()=>{navigation.navigate("Users")}}>
            <View
              style={[
                styles.sectionFooter,
                { borderTopWidth: 1, borderTopColor: colors.light_gray },
              ]}
            >
              <Text style={styles.historyButton}>Operadores</Text>
              <View style={styles.wrapNotify}>
                <View style={styles.badgeNotification}>
                  <Text style={styles.badgeInner}>4</Text>
                </View>
              </View>
              <Ionicons name="chevron-forward" style={styles.arrow} />
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={handleClose}>
            <View
              style={[
                styles.sectionFooter,
                { borderTopWidth: 1, borderTopColor: colors.light_gray },
              ]}
            >
              <Text style={styles.historyButton}>Sair</Text>
              <Ionicons name="chevron-forward" style={styles.arrow} />
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    alignItems: "center",
    backgroundColor: colors.dark_blue,
    justifyContent: "center",
  },
  header: {
    paddingVertical: 34,
  },
  userTitle: {
    fontFamily: fonts.bold,
    color: colors.white,
    fontSize: 18,
    textAlign: "center",
  },
  headerContent: {
    //flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingBottom: 20,
  },
  content: {
    flex: 1,
    width: "100%",
    justifyContent: "flex-start",
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: colors.white,
    paddingTop: 40,
  },
  titleContent: {
    fontSize: 21,
    fontFamily: fonts.regular,
    color: colors.dark_blue,
    textAlign: "left",
    paddingRight: 100,
  },
  wrapContent: {
    flex: 1,

    justifyContent: "center",
    alignItems: "center",
  },
  userName: {
    fontFamily: fonts.bold,
    color: colors.dark_blue,
    fontSize: 16,
  },
  userAgency: {
    fontFamily: fonts.regular,
    color: colors.gray,
    fontSize: 14,
    marginTop: 20,
    textAlign: "center",
  },

  wrapFooter: {
    justifyContent: "flex-end",
    paddingHorizontal: 29,
  },
  sectionFooter: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingVertical: 28,
    alignItems: "center",
  },
  sectionTitle: {
    fontFamily: fonts.regular,
    color: colors.light_blue,
    fontSize: 16,
  },
  historyButton: {
    fontFamily: fonts.regular,
    color: colors.dark_blue,
    fontSize: 16,
  },
  arrow: {
    fontSize: 24,
    color: colors.light_blue,
  },
  wrapNotify: {
    flex: 1,
    justifyContent: "flex-end",
    alignItems: "flex-end",
    paddingRight: 10,
  },
  iconStar: {
    fontSize: 24,
    color: colors.orange,
    justifyContent: "flex-end",
  },
  badgeNotification: {
    backgroundColor: colors.light_blue,
    paddingHorizontal: 7,
    borderRadius: 5,
  },
  badgeInner: {
    fontFamily: fonts.bold,
    color: colors.white,
  },
});
