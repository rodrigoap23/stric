import { useNavigation } from "@react-navigation/core";
import React from "react";
import {
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import about from "../../assets/about.png";
import barcode2 from "../../assets/barcode2.png";
import barcode3 from "../../assets/barcode3.png";
import cross from "../../assets/cross.png";
import regular from "../../assets/regular.png";
import HeaderSection from "../components/HeaderSection";
import ListItem from "../components/ListItem";
import colors from "../styles/colors";
import fonts from "../styles/fonts";

export function Notifications() {
  const navigation = useNavigation();

  function handleLnk(lnk: string) {}
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar />
      <View style={styles.header}>
        <Text style={styles.userTitle}>Martin Ferramentas</Text>
      </View>
      <View style={styles.content}>
        <HeaderSection title={"Notificações"} />
        <View style={styles.wrapNotifications}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate("DepositThree");
            }}
          >
            <ListItem
              icon={barcode2}
              title={"Cobranças"}
              name={"Em aberto"}
              values={"R$ 5.600,00"}
              color={colors.light_blue}
              colorValue={colors.light_blue}
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => {}}>
            <ListItem
              icon={barcode3}
              title={"Aguardando Aprovação"}
              name={"Juliana Costa"}
              values={"R$ 420,00"}
              color={colors.orange}
              colorValue={colors.orange}
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => {}}>
            <ListItem
              icon={about}
              title={"Configurações da Conta"}
              name={"Complete o seu cadastro"}
              values={"Pendente"}
              color={colors.red}
              colorValue={colors.red}
            />
          </TouchableOpacity>
          <ListItem
            icon={barcode3}
            title={"Cobranças"}
            name={"Uma cobrança irá vencer"}
            values={"R$ 12.220,00"}
            color={colors.orange}
            colorValue={colors.orange}
          />
          <ListItem
            icon={regular}
            title={"Cobranças"}
            name={"Materiais e Cia LTDA"}
            values={"R$ 1.600,00"}
            color={colors.light_green}
            colorValue={colors.light_green}
          />
          <ListItem
            icon={cross}
            title={"Cobrança"}
            name={"Cobrança vencida"}
            values={"R$ 1.600,00"}
            color={colors.red}
            colorValue={colors.red}
          />
        </View>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    alignItems: "center",
    backgroundColor: colors.dark_blue,
  },
  header: {
    paddingVertical: 34,
  },
  userTitle: {
    fontFamily: fonts.bold,
    color: colors.white,
    fontSize: 18,
    textAlign: "center",
  },
  content: {
    flex: 1,
    width: "100%",
    justifyContent: "flex-start",
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: colors.white,
    paddingTop: 40,
  },
  wrapNotifications: {
    paddingBottom: 20,
    paddingHorizontal: 29,
    paddingTop: 30,
  },
  value: {
    fontFamily: fonts.regular,
    fontSize: 17,
  },
  valuePositive: {
    fontFamily: fonts.regular,
    fontSize: 17,
    color: colors.green,
  },
  notificationsBoxIcon: {
    alignItems: "center",
    justifyContent: "center",
  },
  notificationsChevron: {
    color: colors.gray,
    fontSize: 16,
  },
});
