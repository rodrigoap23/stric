import { Picker } from "@react-native-community/picker";
import { useNavigation } from "@react-navigation/core";
import React, { useState } from "react";
import {
  Image,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Switch,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { TextInput } from "react-native-gesture-handler";
import { TextInputMask } from "react-native-masked-text";
import eye from "../../assets/eye.png";
import ConfirmButton from "../components/ConfirmButton";
import HeaderSection from "../components/HeaderSection";
import colors from "../styles/colors";
import fonts from "../styles/fonts";

export function AddUser() {
  const [money, setMoney] = useState("0,00");
  const [isEnabled, setIsEnabled] = useState(true);
  const toggleSwitch = () => setIsEnabled((previousState) => !previousState);

  const navigation = useNavigation();


  return (
    <>
      <ScrollView>
        <SafeAreaView style={styles.container}>
          <StatusBar />
          <View style={styles.header}>
            <Text style={styles.userTitle}>Martin Ferramentas</Text>
          </View>
          <View style={styles.content}>
            <HeaderSection
              title="Novo Operador"
              leftIcon={true}
              leftNavigation={"Users"}
              rightIcon={false}
              rightNavigation={"LoggedIn"}
            />

            <View style={styles.wrapInner}>
                <Text style={styles.title}>
                O operador terá permissão para aprovar as todas as movimentações financeiras desta conta?
                </Text>
               
                <View style={styles.wrapBoxes}>
                <TouchableOpacity style={styles.boxActive}>
                  <View style={styles.wrapTile}></View>
                  <Text style={styles.titleBox}>
                   Não
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.box}>
                  <View style={styles.wrapTile}>
                    
                  </View>
                  <Text style={styles.titleBox}>
                     Sim
                  </Text>
                </TouchableOpacity>
              </View>

            </View>
            <View style={styles.wrapForm}>
              <View style={styles.wrapRowThree}>
                <View style={styles.wrapInputCol}>
                  <Text style={styles.label}>Celular</Text>
                  <TextInput
                    keyboardType="numeric"
                    value={"(21) 9823-1238"}
                    style={styles.input}
                  />
                </View>
              </View>
              <View style={styles.wrapRowThree}>
                <View style={styles.wrapInput}>
                  <Text style={styles.label}>Nome</Text>
                  <TextInput
                    
                    style={styles.input} 
                  />
                </View>
              </View>
              <View style={styles.wrapRowThree}>
                <View style={styles.wrapInput}>
                  <Text style={styles.label}>Cpf</Text>
                  <TextInput
                    value="123.456.789-10"
                    style={styles.input} 
                    keyboardType="numeric"
                  />
                </View>
              </View>
              
              <View style={styles.wrapRowThree}>
                <View style={styles.wrapInput}>
                  <Text style={styles.label}>Senha</Text>
                  <TextInput
                    
                    style={styles.input} 
                  />
                </View>
              </View>
              <View style={styles.wrapRowThree}>
                <View style={styles.wrapInput}>
                  <Text style={styles.label}>Repita a senha</Text>
                  <TextInput
                    
                    style={styles.input} 
                  />
                </View>
              </View>

            </View>
            {/* <View style={styles.wrapFooter}>
             
             
            </View> */}
          </View>
        </SafeAreaView>
      </ScrollView>

      <ConfirmButton title="Criar novo operador" navTo={"Users"} />
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    alignItems: "center",
    backgroundColor: colors.dark_blue,
  },
  header: {
    paddingVertical: 34,
  },
  userTitle: {
    fontFamily: fonts.bold,
    color: colors.white,
    fontSize: 18,
    textAlign: "center",
  },
  headerContent: {
    //flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  content: {
    flex: 1,
    width: "100%",
    justifyContent: "flex-start",
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: colors.white,
    paddingTop: 40,
  },
  wrapInner: {
    paddingHorizontal: 29,
    paddingTop: 20,
  },
  title:{
    fontFamily:fonts.regular,
    fontSize:14,
    color:colors.dark_blue,
    lineHeight: 25
  },
 
  backIcon: {
    fontSize: 36,
    color: colors.light_blue,
  },
  cancelButton: {
    fontFamily: fonts.regular,
    color: colors.light_blue,
    fontSize: 16,
  },
  wrapBalance: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    borderWidth: 1,
    borderColor: colors.light_gray,
    borderRadius: 10,
    paddingHorizontal: 20,
    paddingVertical: 15,
  },
  balanceTitle: {
    fontFamily: fonts.regular,
    fontSize: 18,
    color: colors.dark_blue,
  },
  balanceValue: {
    fontFamily: fonts.bold,
    fontSize: 18,
    color: colors.dark_blue,
  },
  icon: {
    width: 24,
    height: 18,
    resizeMode: "contain",
  },
  wrapDepositValue: {
    alignItems: "center",
    paddingVertical: 20,
  },
  depositFormLabel: {
    fontFamily: fonts.regular,
    fontSize: 18,
    color: colors.dark_blue,
    marginBottom: 10,
  },
  inputValue: {
    fontFamily: fonts.regular,
    fontSize: 32,
    color: colors.light_blue,
    borderBottomWidth: 1,
    borderBottomColor: colors.light_gray,
    paddingBottom: 10,
  },
  wrapForm: {
    flex: 1,
    justifyContent: "flex-start",
    paddingHorizontal: 29,
    paddingTop: 10,
  },
  label: {
    fontFamily: fonts.regular,
    fontSize: 14,
    color: colors.dark_blue,
  },
  wrapInput: {},
  wrapInputCol: {
    width: "100%",
  },
  wrapRowOne: {
    width: "100%",
    padding: 0,
    borderBottomWidth: 1,
    borderBottomColor: colors.light_gray,
    marginBottom: 34,
  },
  wrapRowTwo: {
    flexDirection: "row",
    justifyContent: "space-between",
    padding: 0,
    marginBottom: 34,
  },
  wrapRowThree: {
    marginBottom: 34,
  },
  input: {
    fontFamily: fonts.regular,
    fontSize: 16,
    color: colors.gray,
    borderBottomWidth: 1,
    borderBottomColor: colors.light_gray,
    paddingVertical: 5,
  },
  sectionFooter: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingVertical: 34,
    alignItems: "center",
  },
  wrapSwitch: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  sectionLabel: {
    fontFamily: fonts.regular,
    fontSize: 14,
    color: colors.light_blue,
    marginRight: 10,
  },
  containerSwitch: {},
  wrapBoxes: {
    flexDirection: "row",
    justifyContent: "space-between",
    
    paddingVertical: 20,
  },
  wrapFooter: {
    alignItems: "center",
  },
  box: {
    width: "48%",
    height: 88,
    borderRadius: 12,
    justifyContent: "space-between",
    paddingLeft: 23,
    paddingBottom: 20,

    backgroundColor: "#fff",
    shadowColor: "#D8D8D8",
    borderWidth: 0,
    borderColor: colors.light_gray,
    borderBottomWidth: 0,
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.3,
    shadowRadius: 2,
    elevation: 9,
  },
  boxActive: {
    width: "48%",
    height: 88,
    borderRadius: 12,
    justifyContent: "space-between",
    paddingLeft: 23,
    paddingBottom: 15,
    borderWidth: 5,
    borderColor: colors.light_blue,
  },
  wrapTile: {
    justifyContent: "center",
    alignItems: "center",
  },
  tile: {
    width: 39,
    height: 4,
    backgroundColor: colors.light_blue,
    borderRadius: 12,
  },
  titleBox: {
    fontFamily: fonts.regular,
    fontSize: 16,
    color: colors.dark_blue,
  },
  warnLabel: {
    fontFamily: fonts.regular,
    color: colors.gray,
    fontSize: 14,
    paddingVertical: 33,
  },
  wrapButton: {
    width: "100%",
    alignItems: "center",
    justifyContent: "flex-end",
  },
  Button: {
    padding: 18,
    flexDirection: "row",
    justifyContent: "space-between",
    width: "100%",
    alignItems: "center",
    backgroundColor: colors.light_blue,
  },
  ButtonText: {
    color: colors.white,
    fontSize: 22,
    fontFamily: fonts.regular,
  },
  ButtonIcon: {
    fontSize: 16,
    color: colors.white,
    marginTop: 0,
  },
});
