import { Ionicons } from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/core";
import React, { useState } from "react";
import {
  Image,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { TextInputMask } from "react-native-masked-text";
import logoWhite from "../../assets/logoWhite.png";
import colors from "../styles/colors";
import fonts from "../styles/fonts";

export function ForgotenAccountOne() {
  const [hideButton, setHideButton] = useState(true);

  function changeButton() {
    setHideButton(!hideButton);
  }

  const [cnpj, setCnpj] = useState("");

  const navigation = useNavigation();

  function handleClose() {
    navigation.navigate("Lobby");
  }
  function handleNext() {
    navigation.navigate("LoginAccountTwo");
  }
  function handleCreateAccount() {
    navigation.navigate("CreateAccountOne");
  }
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar />
      <Image source={logoWhite} style={styles.imageLogo} />
      <View style={styles.content}>
        <View style={styles.header}>
          <Text style={styles.title}>Para entrar, digite seu CNPJ</Text>

          <TouchableOpacity onPress={handleClose}>
            <Ionicons name="close" style={styles.closeIcon} />
          </TouchableOpacity>
        </View>
        <TextInputMask
          style={styles.inputForm}
          type="cnpj"
          options={{
            maskType: "BRL",
          }}
          value={cnpj}
          onChangeText={(text) => setCnpj(text)}
          onChange={changeButton}
        />

        <View style={styles.wrapFooter}>
          <View style={styles.wrapFooterContent}>
            <Text style={styles.signinText}>É novo aqui?</Text>
            <TouchableOpacity onPress={handleCreateAccount}>
              <Text style={styles.signinlink}>Crie sua conta</Text>
            </TouchableOpacity>
          </View>

          {/* <View style={styles.wrapButton}>
                                <View style={styles.wrapButton}>
                                    <View style={styles.ButtonInactive}>
                                        <Text style={styles.ButtonText}>
                                            Continuar
                                        </Text>
                                        <Ionicons 
                                            name="chevron-forward" 
                                            style={styles.ButtonIcon} 
                                            />
                                    </View>
                                </View>
                            </View> */}

          <TouchableOpacity onPress={handleNext} style={styles.wrapButton}>
            <View style={styles.wrapButton}>
              <View style={styles.Button}>
                <Text style={styles.ButtonText}>Continuar</Text>
                <Ionicons name="chevron-forward" style={styles.ButtonIcon} />
              </View>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    alignItems: "center",
    backgroundColor: colors.dark_blue,
  },
  imageLogo: {
    width: 62,
    height: 23,
    marginTop: 38,
    marginBottom: 38,
  },
  content: {
    flex: 1,
    width: "100%",
    justifyContent: "flex-start",
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: colors.white,
    paddingTop: 40,
  },
  header: {
    // flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: 29,
  },
  title: {
    fontSize: 21,
    fontFamily: fonts.regular,
    color: colors.dark_blue,
  },
  closeIcon: {
    fontSize: 36,
    color: colors.light_blue,
    marginTop: -6,
  },
  form: {
    justifyContent: "center",
    alignItems: "center",
  },
  inputForm: {
    width: "100%",
    color: colors.light_blue,
    fontSize: 30,
    marginTop: 32,
    marginHorizontal: 29,
    fontFamily: fonts.regular,

    borderLeftColor: colors.light_gray,
  },
  wrapFooter: {
    flex: 1,
    justifyContent: "flex-end",
  },
  wrapFooterContent: {
    flexDirection: "row",
    paddingHorizontal: 29,
    paddingVertical: 17,
  },
  signinText: {
    fontSize: 19,
    fontFamily: fonts.regular,
    color: colors.light_gray,
  },
  signinlink: {
    paddingLeft: 10,
    fontSize: 19,
    fontFamily: fonts.regular,
    color: colors.light_gray,
    textDecorationLine: "underline",
  },
  wrapButton: {
    width: "100%",
    alignItems: "center",
  },
  wrapButtonInactive: {
    width: "100%",
    alignItems: "center",
  },
  ButtonInactive: {
    padding: 18,
    paddingHorizontal: 29,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    width: "100%",
    backgroundColor: colors.light_gray,
  },
  Button: {
    padding: 18,
    paddingHorizontal: 29,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    width: "100%",
    backgroundColor: colors.light_blue,
  },
  ButtonText: {
    color: colors.white,
    fontSize: 22,
    fontFamily: fonts.regular,
  },
  ButtonIcon: {
    fontSize: 36,
    color: colors.white,
    marginTop: -1,
  },
});
