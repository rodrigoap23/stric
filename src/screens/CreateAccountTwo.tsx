import { useNavigation } from "@react-navigation/core";
import React, { useState } from "react";
import {
  Animated,
  Image,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  View,
} from "react-native";
import { TextInputMask } from "react-native-masked-text";
import logoWhite from "../../assets/logoWhite.png";
import ConfirmButton from "../components/ConfirmButton";
import HeaderSection from "../components/HeaderSection";
import colors from "../styles/colors";
import fonts from "../styles/fonts";

export function CreateAccountTwo() {
  const [cnpj, setCnpj] = useState("");

  const navigation = useNavigation();

  function handleClose() {
    navigation.navigate("Lobby");
  }
  function handleNext() {
    navigation.navigate("CreateAccountThree");
  }
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar />
      <Image source={logoWhite} style={styles.imageLogo} />
      <Animated.View style={styles.content}>
        <HeaderSection
          title={"Entre com seu CNPJ"}
          rightIcon={true}
          rightNavigation={"Lobby"}
        />
        <TextInputMask
          style={styles.inputForm}
          type="cnpj"
          options={{
            maskType: "BRL",
          }}
          value={cnpj}
          onChangeText={(text) => setCnpj(text)}
        />
        <View style={styles.Footer}>
          <ConfirmButton title={"Continuar"} navTo={"CreateAccountThree"} />
        </View>
      </Animated.View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    alignItems: "center",
    backgroundColor: colors.dark_blue,
  },
  imageLogo: {
    width: 62,
    height: 23,
    marginTop: 38,
    marginBottom: 38,
  },
  content: {
    flex: 1,
    width: "100%",
    justifyContent: "flex-start",
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: colors.white,
    paddingTop: 40,
  },
  form: {
    justifyContent: "center",
    alignItems: "center",
  },
  inputForm: {
    width: "100%",
    color: colors.light_blue,
    fontSize: 30,
    marginTop: 32,
    marginHorizontal: 29,
    fontFamily: fonts.regular,
    borderLeftWidth: 1,
    borderLeftColor: colors.light_gray,
  },
  Footer: {
    backgroundColor: colors.white,
    flex: 1,
    justifyContent: "flex-end",
    width: "100%",
  },
});
