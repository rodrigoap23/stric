import { Ionicons } from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/core";
import React from "react";
import {
  Dimensions,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import copy from "../../assets/copy.png";
import keypass from "../../assets/keypass.png";
import qrcode from "../../assets/qr-code.png";
import BoxBalance from "../components/BoxBalance";
import BoxItem from "../components/BoxItem";
import HeaderSection from "../components/HeaderSection";
import colors from "../styles/colors";
import fonts from "../styles/fonts";

export function Pix() {
  const navigation = useNavigation();

  
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar />
      <View style={styles.header}>
        <Text style={styles.userTitle}>Martin Ferramentas</Text>
      </View>
      <View style={styles.content}>
        <HeaderSection
          title={"Transferir via Pix"}
          leftIcon={true}
          leftNavigation={"LoggedIn"}
          rightIcon={false}
          rightNavigation={"LoggedIn"}
        />
        <View style={styles.innerContent}>
          <BoxBalance balance={"24.895,36"} />
          <View style={styles.wrapBoxes}>
            <BoxItem
              title={"Pix com QR"}
              useImageIcon={true}
              imageIcon={qrcode}
              navTo={"PixQrScanner"}
            />
            <BoxItem
              title={"Pix chave"}
              useImageIcon={true}
              imageIcon={keypass}
              navTo={"PixKeyTransfer"}
            />
            <BoxItem
              title={"Copia e cola"}
              useImageIcon={true}
              imageIcon={copy}
              navTo={"PixKeyHash"}
            />
          </View>
        </View>
        <View style={styles.wrapFooter}>
          <View style={styles.sectionFooter}>
            <Text style={styles.sectionTitle}>Configurações</Text>
          </View>
          <View style={styles.sectionFooter}>
            <TouchableOpacity>
              <Text style={styles.historyButton}>Minhas chaves</Text>
            </TouchableOpacity>
            <Ionicons name="chevron-forward" style={styles.arrow} />
          </View>
          <View
            style={[
              styles.sectionFooter,
              { borderTopWidth: 1, borderTopColor: colors.light_gray },
            ]}
          >
            <TouchableOpacity>
              <Text style={styles.historyButton}>Criar nova chave</Text>
            </TouchableOpacity>
            <Ionicons name="chevron-forward" style={styles.arrow} />
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    alignItems: "center",
    backgroundColor: colors.dark_blue,
    justifyContent: "center",
  },
  header: {
    paddingVertical: 34,
  },
  userTitle: {
    fontFamily: fonts.bold,
    color: colors.white,
    fontSize: 18,
    textAlign: "center",
  },
  headerContent: {
    //flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingBottom: 20,
  },
  content: {
    flex: 1,
    width: "100%",
    justifyContent: "flex-start",
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: colors.white,
    paddingTop: 40,
  },
  titleContent: {
    fontSize: 21,
    fontFamily: fonts.regular,
    color: colors.dark_blue,
    textAlign: "left",
    paddingRight: 100,
  },
  backIcon: {
    fontSize: 36,
    color: colors.light_blue,
    marginLeft: -5,
  },
  cancelButton: {
    fontFamily: fonts.regular,
    color: colors.light_blue,
    fontSize: 16,
  },

  wrapBoxes: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingVertical: 20,
  },

  box: {
    width: 117,
    height: 128,
    borderWidth: 1,
    borderRadius: 12,
    borderColor: "#e3e3e3",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: colors.white,
    paddingHorizontal: 13,
    paddingBottom: 13,
    // marginRight: 15,
  },
  boxTile: {
    width: 40,
    height: 4,
    backgroundColor: colors.light_blue,
  },
  boxIcon: {
    width: 30,
    height: 30,
    resizeMode: "contain",
  },
  boxTitle: {
    fontFamily: fonts.regular,
    color: colors.dark_blue,
    fontSize: Dimensions.get("screen").width * 0.036,
  },

  innerContent: {
    paddingHorizontal: 29,
    paddingTop: 20,
  },
  wrapFooter: {
    flex: 1,
    justifyContent: "flex-end",
    paddingHorizontal: 29,
  },
  sectionFooter: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingVertical: 28,
    alignItems: "center",
  },
  sectionTitle: {
    fontFamily: fonts.regular,
    color: colors.light_blue,
    fontSize: 16,
  },
  historyButton: {
    fontFamily: fonts.regular,
    color: colors.dark_blue,
    fontSize: 16,
  },
  arrow: {
    fontSize: 24,
    color: colors.light_blue,
  },
});
