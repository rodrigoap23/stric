import { useNavigation } from "@react-navigation/core";
import React from "react";
import { SafeAreaView, StatusBar, StyleSheet, Text, View } from "react-native";
import line from "../../assets/line.png";
import list from "../../assets/list.png";
import profile from "../../assets/profile.png";
import star from "../../assets/star-blue-outline.png";
import BoxBalance from "../components/BoxBalance";
import BoxItem from "../components/BoxItem";
import HeaderSection from "../components/HeaderSection";
import colors from "../styles/colors";
import fonts from "../styles/fonts";

export function TransferTedOne() {
  const navigation = useNavigation();

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar />
      <View style={styles.header}>
        <Text style={styles.userTitle}>Martin Ferramentas</Text>
      </View>
      <View style={styles.content}>
        <HeaderSection
          title="Transferir via TED"
          leftIcon={true}
          leftNavigation={"Transfer"}
          rightIcon={false}
          rightNavigation={"LoggedIn"}
        />
        <View style={styles.innerContent}>
          <BoxBalance balance="24.895,36" />
          <View style={styles.wrapBoxes}>
            <BoxItem
              useImageIcon={true}
              imageIcon={profile}
              title="Nova Conta"
              navTo={"TransfTedNewContact"}
            />
            <BoxItem
              useImageIcon={true}
              imageIcon={line}
              title="Contatos"
              navTo={"TransfContact"}
            />
            <BoxItem
              useImageIcon={true}
              imageIcon={list}
              title="Recentes"
              navTo={"TransfRecents"}
            />
          </View>
          <View style={styles.wrapBoxes}>
            <BoxItem
              useImageIcon={true}
              imageIcon={star}
              title="Favoritos"
              navTo={"TransfFavorites"}
            />
          </View>
        </View>
        <View style={styles.wrapFooter}></View>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    alignItems: "center",
    backgroundColor: colors.dark_blue,
  },
  header: {
    paddingVertical: 34,
  },
  userTitle: {
    fontFamily: fonts.bold,
    color: colors.white,
    fontSize: 18,
    textAlign: "center",
  },
  content: {
    flex: 1,
    width: "100%",
    justifyContent: "flex-start",
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: colors.white,
    paddingTop: 40,
    // paddingHorizontal: 29,
  },
  innerContent: {
    paddingHorizontal: 29,
    paddingTop: 20,
  },

  wrapBoxes: {
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "space-between",
    paddingTop: 20,
  },

  wrapFooter: {
    flex: 1,
    justifyContent: "flex-end",
    paddingHorizontal: 29,
  },
});
