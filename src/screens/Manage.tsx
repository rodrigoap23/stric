import { Ionicons } from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/core";
import React from "react";
import {
  Dimensions,
  Image,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import grafics from "../../assets/grafic.png";
import colors from "../styles/colors";
import fonts from "../styles/fonts";

export function Manage() {
  const navigation = useNavigation();

  function handleLnk(lnk: string) {}
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar />
      <View style={styles.header}>
        <Text style={styles.userTitle}>Martin Ferramentas</Text>
      </View>
      <View style={styles.content}>
        <View style={styles.headerContent}>
          <TouchableOpacity onPress={() => {}}>
            <Ionicons name="chevron-back" style={styles.backButton} />
          </TouchableOpacity>
          <Text style={styles.titleContent}>Gráficos</Text>
          <TouchableOpacity onPress={() => {}}>
            <Text style={styles.cancelButton}>Cancelar</Text>
          </TouchableOpacity>
        </View>

        <View style={styles.contentGrafics}>
          <Image source={grafics} style={styles.grafics} />
        </View>

        <View style={styles.contentMonth}>
          <Text style={styles.month}>Maio</Text>
          <Text style={styles.month}>Junho</Text>
          <Text style={styles.month}>Julho</Text>
        </View>
        <ScrollView>
          <View style={styles.wrapContent}>
            <View style={styles.contentBox}>
              <Text style={styles.title}>
                Cobranças {"\n"}
                em aberto
              </Text>
              <Text style={styles.value}>4</Text>
            </View>
            <View style={styles.contentBox}>
              <Text style={styles.title}>
                Taxas de {"\n"}
                boletos pagos
              </Text>
              <Text style={styles.value}>89%</Text>
            </View>
            <View style={styles.contentBox}>
              <Text style={styles.title}>
                Faturamento {"\n"}
                médio mensal
              </Text>
              <Text style={styles.value}>R$ 51.930,31</Text>
            </View>
            <View style={styles.contentBox}>
              <Text style={styles.title}>
                Faturamento {"\n"}
                anual
              </Text>
              <Text style={styles.value}>R$ 250.930,92</Text>
            </View>
          </View>
        </ScrollView>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    backgroundColor: colors.dark_blue,
  },

  header: {
    paddingVertical: 34,
  },
  userTitle: {
    fontFamily: fonts.bold,
    color: colors.white,
    fontSize: 18,
    textAlign: "center",
  },
  headerContent: {
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    flexDirection: "row",
    justifyContent: "space-between",
    backgroundColor: colors.light_blue,
    paddingTop: 40,
    paddingHorizontal: 20,
  },
  content: {
    flex: 1,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: colors.white,
  },
  contentGrafics: {
    height: Dimensions.get("window").height * 0.27,
    backgroundColor: colors.light_blue,
    justifyContent: "flex-end",
    alignItems: "center",
  },
  grafics: {
    flex: 1,
    resizeMode: "cover",
    width: "105%",
  },
  titleContent: {
    fontSize: 21,
    fontFamily: fonts.regular,
    color: colors.white,
    textAlign: "left",
    paddingRight: 180,
    paddingBottom: 15,
  },

  backButton: {
    fontSize: 20,
    color: colors.white,
  },
  cancelButton: {
    fontFamily: fonts.regular,
    fontSize: 16,
    color: colors.white,
  },
  contentMonth: {
    flexDirection: "row",
    alignContent: "center",
    justifyContent: "center",
    paddingVertical: 20,
  },
  month: {
    fontFamily: fonts.bold,
    fontSize: 16,
    marginHorizontal: 39,
  },
  wrapContent: {
    paddingHorizontal: 29,
  },
  contentBox: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    borderWidth: 1,
    borderColor: colors.light_blue,
    borderRadius: 15,
    paddingHorizontal: 12,
    paddingVertical: 13,
    marginBottom: 20,
  },
  title: {
    fontFamily: fonts.regular,
    fontSize: 16,
  },
  value: {
    fontFamily: fonts.regular,
    fontSize: 26,
    color: colors.light_blue,
  },
});
