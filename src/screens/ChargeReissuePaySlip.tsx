import { Ionicons } from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/core";
import React from "react";
import {
  Image,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import paidIcon from "../../assets/025-email.png";
import copy from "../../assets/copy2.png";
import HeaderTransferSection from "../components/HeaderTransferSection";
import colors from "../styles/colors";
import fonts from "../styles/fonts";

export function ChargeReissuePaySlip() {
  const navigation = useNavigation();

  function handleClose() {
    navigation.navigate("LoggedIn");
  }

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar />
      <View style={styles.header}>
        <Text style={styles.userTitle}>Martin Ferramentas</Text>
      </View>
      <View style={styles.content}>
        <HeaderTransferSection
          sectionImage={paidIcon}
          title={"Seu boleto de cobrança foi gerado"}
          leftNavigation={"ChargeOpen"}
        />
        <View style={styles.wrapBoxes}>
          <View style={styles.box}>
            <View style={styles.boxTile}></View>

            <View style={styles.wrapBoxCode}>
              <Text style={styles.boxCode}>
                34191.79001 01043.510047{"\n"}
                91020.150008 3 86190026000
              </Text>
              <Image style={styles.boxIcon} source={copy} />
            </View>
          </View>
        </View>
        <View style={styles.wrapFooter}>
          <ScrollView style={styles.ScrollView}>
            <View>
              <View style={styles.wrapDate}>
                <Text style={styles.LabelText}>Vencimento</Text>
                <Text style={styles.Text}>07/02/2022</Text>
              </View>
              <View style={styles.wrapValue}>
                <Text style={styles.LabelText}>Valor do depósito</Text>
                <Text style={styles.Text}>R$ 3.000</Text>
              </View>
              
            </View>
          </ScrollView>
        </View>
      </View>
      <TouchableOpacity onPress={() => {}} style={styles.wrapButton}>
        <View style={styles.wrapButton}>
          <View style={styles.Button}>
            <Text style={styles.ButtonText}>Compartilhar</Text>
            <Ionicons name="chevron-forward" style={styles.ButtonIcon} />
          </View>
        </View>
      </TouchableOpacity>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  ScrollView: {},
  container: {
    flex: 1,
    width: "100%",
    alignItems: "center",
    backgroundColor: colors.dark_blue,
  },
  header: {
    paddingVertical: 34,
  },
  userTitle: {
    fontFamily: fonts.bold,
    color: colors.white,
    fontSize: 18,
    textAlign: "center",
  },
  content: {
    flex: 1,
    width: "100%",

    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: colors.white,
    paddingTop: 25,
    paddingHorizontal: 29,
  },

  wrapBoxes: {
    flexDirection: "row",
    alignItems: "flex-start",
    paddingVertical: 20,
  },
  box: {
    width: "100%",
    borderRadius: 12,

    borderColor: "#e3e3e3",

    alignItems: "center",
    backgroundColor: colors.white,

    shadowColor: "#dcdcdc",
   borderBottomWidth: 0,
   shadowOffset: { width: 5, height: 9 },
   shadowOpacity: 0.1,
   shadowRadius: 2,
   elevation: 10,
  },
  boxTile: {
    width: 40,
    height: 4,
    backgroundColor: colors.orange,
  },

  boxIcon: {
    width: 19,
    height: 19,
    resizeMode: "contain",
  },
  boxCode: {
    fontFamily: fonts.regular,
    color: colors.dark_blue,
    fontSize: 15,
    textAlign: "center",
    paddingVertical: 20,
    paddingHorizontal: 30,
  },
  wrapBoxCode: {
    paddingHorizontal: 38,

    flexDirection: "row",
    alignItems: "center",
  },
  wrapFooter: {
    flex: 1,
  },
  wrapDate: {
    marginTop: 38,
  },
  wrapValue: {
    marginTop: 24,
  },
  wrapdescription: {
    marginTop: 24,
  },
  LabelText: {
    fontFamily: fonts.regular,
    fontSize: 14,
    color: colors.dark_blue,
  },
  Text: {
    fontFamily: fonts.regular,
    fontSize: 16,
    color: colors.gray,
    paddingVertical: 4.5,
  },
  sectionFooter: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  warnLabel: {
    fontFamily: fonts.regular,
    color: colors.gray,
    fontSize: 14,
    textAlign: "center",
    marginTop: 66,
    marginBottom: 34,
  },

  wrapButton: {
    width: "100%",
    alignItems: "center",
    justifyContent: "flex-end",
  },
  Button: {
    padding: 18,
    flexDirection: "row",
    justifyContent: "space-between",
    width: "100%",
    alignItems: "center",
    backgroundColor: colors.light_blue,
  },
  ButtonText: {
    color: colors.white,
    fontSize: 22,
    fontFamily: fonts.regular,
  },
  ButtonIcon: {
    fontSize: 16,
    color: colors.white,
    marginTop: 0,
  },
});
