export default{
   bold: "Flexo-Bold",
   medium:      "Flexo-Medium",
   regular: "Flexo-Regular",
};