export default{
    blue: '#2F437C',
    dark_blue: '#142249',
    medium_blue: '#56B9BE',
    light_blue: '#38D2D9',
    gray: '#B5B7BD',
    light_gray: '#E3E3E3',
    white: '#FFFFFF',
    green: '#74BF61',
    light_green: '#4AD395',
    red: '#E62C25',
    orange: '#F09B12'
}