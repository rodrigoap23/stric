import { useNavigation } from "@react-navigation/core";
import React from "react";
import {
  Dimensions,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import colors from "../styles/colors";
import fonts from "../styles/fonts";

const BoxItem = (props: any) => {
  const navigation = useNavigation();
  return (
    <TouchableOpacity
      onPress={() => {
        navigation.navigate(props.navTo);
      }}
      style={styles.box}
    >
      <View style={styles.boxTile}></View>
      {props.useImageIcon == true ? (
        <Image style={styles.boxIcon} source={props.imageIcon} />
      ) : (
        <Text style={styles.textIcon}>{props.text}</Text>
      )}

      <Text style={[styles.boxTitle]}>{props.title}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  box: {
    width: Dimensions.get("screen").width / 4 + 10,
    // width: 117,
     height: 128,
    //borderWidth: 1,
    borderRadius: 12,
    borderColor: "#e3e3e3",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: colors.white,
    paddingHorizontal: 13,
    paddingBottom: 13,
    // marginRight: 15,

     // borderWidth: 0,
    // borderColor: colors.light_gray,
   //backgroundColor: "#fff",
    shadowColor: "#a9a9a9",
    borderBottomWidth: 0,
    shadowOffset: { width: 5, height: 9 },
    shadowOpacity: 0.1,
    shadowRadius: 2,
    elevation: 10,
  },
  boxTile: {
    width: 40,
    height: 4,
    backgroundColor: colors.light_blue,
  },
  boxIcon: {
    width: 30,
    height: 30,
    resizeMode: "contain",
  },
  textIcon: {
    width: 30,
    height: 30,
  },
  boxTitle: {
    width: "100%",
    fontFamily: fonts.regular,
    color: colors.dark_blue,
    fontSize: Dimensions.get("screen").width * 0.035,
  },
});
export default BoxItem;
