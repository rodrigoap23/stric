import { Ionicons } from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/core";
import React from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import colors from "../styles/colors";
import fonts from "../styles/fonts";

const ConfirmButton = (props: any) => {
  const navigation = useNavigation();
  return (
    <TouchableOpacity
      onPress={() => {
        navigation.navigate(props.navTo, {
          paramValue: props.paramValue,
          paramUser: props.paramUser,
          
        });
      }}
      style={styles.wrapButton}
    >
      <View style={styles.buttonInner}>
        <View style={styles.Button}>
          <Text style={styles.ButtonText}>{props.title}</Text>
          <Ionicons name="chevron-forward" style={styles.ButtonIcon} />
        </View>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  wrapButton: {
    //flex: 1,
    width: "100%",
    alignItems: "center",
    justifyContent: "flex-end",
  },
  buttonInner: {
    width: "100%",
  },
  Button: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",

    backgroundColor: colors.light_blue,
    paddingVertical: 18,
    paddingHorizontal: 28,
  },
  ButtonText: {
    color: colors.white,
    fontSize: 22,
    fontFamily: fonts.regular,
  },
  ButtonIcon: {
    fontSize: 20,
    color: colors.white,
  },
});

export default ConfirmButton;
