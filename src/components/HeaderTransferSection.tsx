import { Ionicons } from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/core";
import React from "react";
import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import logo from "../../assets/logo.png";
import colors from "../styles/colors";
import fonts from "../styles/fonts";

const HeaderTransferSection = (props: any) => {
  const navigation = useNavigation();
  return (
    <>
      <View style={styles.headerContent}>
        <View style={styles.titleContent}>
          <Image source={logo} style={styles.imageLogo} />
        </View>
        <TouchableOpacity
          onPress={() => {
            navigation.navigate(props.leftNavigation);
          }}
        >
          <Ionicons name="close" style={styles.cancelButton} />
        </TouchableOpacity>
      </View>
      <View style={styles.wrapContent}>
        <View style={styles.wrapImg}>
          <Image source={props.sectionImage} style={styles.img} />
        </View>
        <View style={styles.contentTitle}>
          <Text style={styles.title}>{props.title}</Text>
          {props.value == "" || props.value == null ? 
          <></>
          :
          <Text style={styles.value}>R$ {props.value}</Text>
        }
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  headerContent: {
    //flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingHorizontal: 29,
  },
  titleContent: {
    alignItems: "center",
    justifyContent: "center",
    width: "100%",
  },
  imageLogo: {
    width: 85,
    height: 32,
    resizeMode: "contain",
  },
  backIcon: {
    fontSize: 30,
    color: colors.light_blue,
  },
  cancelButton: {
    fontFamily: fonts.regular,
    color: colors.light_blue,
    fontSize: 35,
  },
  wrapContent: {},
  wrapImg: {
    alignItems: "center",
    paddingVertical: 35.5,
  },
  img: {
    width: 113,
    height: 125,
    resizeMode: "contain",
  },
  contentTitle: {
    alignItems: "center",
  },
  title: {
    fontFamily: fonts.regular,
    color: colors.dark_blue,
    fontSize: 22,
  },
  value: {
    fontFamily: fonts.regular,
    fontSize: 32,
    color: colors.light_blue,
    marginTop: 12,
  },
});

export default HeaderTransferSection;
