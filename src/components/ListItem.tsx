import { Ionicons } from "@expo/vector-icons";
import React from "react";
import { Image, StyleSheet, Text, View } from "react-native";
import colors from "../styles/colors";
import fonts from "../styles/fonts";


import trash from "../../assets/trash.png";
import edit from "../../assets/Edit.png";
import { TouchableOpacity } from "react-native-gesture-handler";
const ListItem = (props: any) => {
  return (
    <View style={styles.notifications}>
      <View style={styles.notificationsTypeBox}>
        {props.useInitials == true ? (
          <Text style={styles.notificationsTypeInitial}>{props.icon}</Text>
        ) : (
          <Image style={styles.notificationsTypeIcon} source={props.icon} />
        )}
      </View>
      <View style={styles.notificationsInner}>
        <Text style={styles.notificationsTitleType}>{props.title}</Text>
        <Text style={[styles.notificationsTitle, { color: props.color }]}>
          {props.name}
        </Text>
      </View>
      
     
      
      {props.editItem == true ? (
      <View style={{flexDirection:"row",justifyContent:"space-between", alignItems:"center"}}>
      
      <TouchableOpacity onPress={props.deleteFunction} style={[styles.notificationsValue, {marginRight:36}]}>
      <Image style={styles.icon} source={trash}/>
      </TouchableOpacity>
     
      <TouchableOpacity onPress={props.editFunction} style={styles.notificationsBoxIcon}>
        <Image style={styles.icon} source={edit}/>
      </TouchableOpacity>
    </View>

      ):(
        <>
      <View style={styles.notificationsValue}>
        <Text style={[styles.notificationsValue, { color: props.colorValue }]}>
          {props.values}
        </Text>
      </View>
      <View style={styles.notificationsBoxIcon}>
        <Ionicons style={styles.notificationsChevron} name="chevron-forward" />
      </View>
      </>
      )}
    </View>




  );
};

const styles = StyleSheet.create({
  notifications: {
    flexDirection: "row",
    justifyContent: "space-between",

    paddingVertical: 10,
  },
  notificationsTypeBox: {
    borderWidth: 1,
    borderColor: colors.light_gray,
    borderRadius: 10,
    backgroundColor: colors.white,
    justifyContent: "center",
    alignItems: "center",
    width: 45,
    height: 45,
  },
  notificationsTypeIcon: {
    width: 23,
    height: 18,
    resizeMode: "contain",
  },
  notificationsTypeInitial: {
    fontFamily: fonts.medium,
    color: colors.light_blue,
    fontSize: 17,
    textTransform: "uppercase",
  },
  notificationsTitleType: {
    fontFamily: fonts.regular,
    color: colors.gray,
  },
  notificationsInner: {
    textAlign: "left",
    flex: 1,
    paddingLeft: 15,
  },
  notificationsTitle: {
    fontFamily: fonts.regular,
    fontSize: 17,
    color: colors.dark_blue,
  },
  notificationsValue: {
    alignItems: "flex-end",
    justifyContent: "center",
    fontFamily: fonts.regular,
    fontSize: 16,
  },
  notificationsBoxIcon: {
    alignItems: "center",
    justifyContent: "center",
  },
  notificationsChevron: {
    color: colors.gray,
    fontSize: 16,
  },
  icon:{
    width: 20,
    height: 20,
    resizeMode: "contain",
  }
});

export default ListItem;
