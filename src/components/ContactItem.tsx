import { Ionicons } from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/native";
import React from "react";
import { Image, StyleSheet, Text, View } from "react-native";
import starActive from "../../assets/starActive.png";
import starInactive from "../../assets/starInactive.png";
import colors from "../styles/colors";
import fonts from "../styles/fonts";

const ContactItem = (props: any) => {
  const navigation = useNavigation();
  return (
    <View style={styles.notifications}>
      <View style={styles.notificationsTypeBox}>
        {/* <Image style={styles.notificationsTypeIcon} source={props.icon}/> */}
        <Text style={styles.initials}>{props.initials}</Text>
      </View>
      <View style={styles.notificationsInner}>
        <Text style={[styles.notificationsTitle, { color: props.color }]}>
          {props.name}
        </Text>
        <View style={styles.favoriteIcon}>
          {props.favorite == true ? (
            <Image style={styles.icon} source={starActive} />
          ) : (
            <Image style={styles.icon} source={starInactive} />
          )}
        </View>
      </View>

      <View style={styles.notificationsBoxIcon}>
        <Ionicons style={styles.notificationsChevron} name="chevron-forward" />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  notifications: {
    flexDirection: "row",
    justifyContent: "space-between",

    paddingVertical: 10,
  },
  notificationsTypeBox: {
    borderWidth: 1,
    borderColor: colors.light_gray,
    borderRadius: 10,
    backgroundColor: colors.white,
    justifyContent: "center",
    alignItems: "center",
    width: 45,
    height: 45,
  },
  notificationsTypeIcon: {
    width: 23,
    height: 18,
    resizeMode: "contain",
  },
  notificationsTitleType: {
    fontFamily: fonts.regular,
    color: colors.gray,
  },
  notificationsInner: {
    flexDirection: "row",
    textAlign: "left",
    flex: 1,
    paddingLeft: 15,
    justifyContent: "space-between",
    alignItems: "center",
  },
  notificationsTitle: {
    fontFamily: fonts.regular,
    fontSize: 17,
    color: colors.dark_blue,
  },
  favoriteIcon: {
    paddingHorizontal: 32,
  },
  notificationsBoxIcon: {
    alignItems: "center",
    justifyContent: "center",
  },
  notificationsChevron: {
    color: colors.gray,
    fontSize: 16,
  },
  icon: {
    width: 17,
    height: 17,
    resizeMode: "contain",
  },
  initials: {
    fontFamily: fonts.medium,
    color: colors.light_blue,
    fontSize: 17,
    textTransform: "uppercase",
  },
});

export default ContactItem;
