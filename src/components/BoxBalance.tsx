import React from "react";
import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import eye from "../../assets/eye.png";
import colors from "../styles/colors";
import fonts from "../styles/fonts";

const BoxBalance = (props: any) => {
  return (
    <View style={styles.wrapBalance}>
      <Text style={styles.balanceTitle}>Saldo:</Text>
      <Text style={styles.balanceValue}>R$ {props.balance}</Text>
      <TouchableOpacity>
        <Image style={styles.icon} source={eye} />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  wrapBalance: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    borderWidth: 1,
    borderColor: colors.light_gray,
    borderRadius: 10,
    paddingHorizontal: 20,
    paddingVertical: 15,
  },
  balanceTitle: {
    fontFamily: fonts.regular,
    fontSize: 18,
    color: colors.dark_blue,
  },
  balanceValue: {
    fontFamily: fonts.bold,
    fontSize: 18,
    color: colors.dark_blue,
  },
  icon: {
    width: 24,
    height: 18,
    resizeMode: "contain",
  },
});

export default BoxBalance;
