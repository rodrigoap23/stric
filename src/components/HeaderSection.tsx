import { Ionicons } from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/core";
import React from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import colors from "../../src/styles/colors";
import fonts from "../../src/styles/fonts";

const HeaderSection = (props: any) => {
  const navigation = useNavigation();
  return (
    <View style={styles.headerContent}>
      {props.leftIcon == true ? (
        <TouchableOpacity
          onPress={() => {
            navigation.navigate(props.leftNavigation);
          }}
        >
          <Ionicons name="chevron-back" style={styles.backIcon} />
        </TouchableOpacity>
      ) : (
        <></>
      )}

      <Text style={styles.titleContent}>{props.title}</Text>
      {props.rightIcon == false ? (
        <TouchableOpacity
          onPress={() => {
            navigation.navigate(props.rightNavigation);
          }}
        >
          <Text style={styles.cancelButton}>Cancelar</Text>
        </TouchableOpacity>
      ) : (
        <></>
      )}
      {props.rightIcon == true ? (
        <TouchableOpacity
          onPress={() => {
            navigation.navigate(props.rightNavigation);
          }}
        >
          <Text style={styles.cancelButton}>
            <Ionicons name="close" style={styles.cancelIcon} />
          </Text>
        </TouchableOpacity>
      ) : (
        <></>
      )}
      {props.search == true ?
      <TouchableOpacity onPress={()=>{navigation.navigate(props.searchScreen)}} style={styles.wrapSearch}>
        <Ionicons name="search" style={styles.searchIcon}/>
      </TouchableOpacity>
      :
      <></>
      }
      {props.addButton != null ?
      <TouchableOpacity onPress={()=>{navigation.navigate(props.addButton)}} style={styles.wrapSearch}>
        <Ionicons name="add-circle" style={styles.addIcon}/>
      </TouchableOpacity>
      :
      <></>
      }
    </View>
  );
};

const styles = StyleSheet.create({
  headerContent: {
    // borderWidth: 1,
    //flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingRight: 29,
    paddingLeft: 19,
  },
  titleContent: {
    flex: 1,
    fontSize: 21,
    fontFamily: fonts.regular,
    // borderWidth:1,
    borderColor: "red",
    color: colors.dark_blue,
    // textAlign: "left",
    paddingLeft: 10,
  },
  backIcon: {
    fontSize: 36,
    color: colors.light_blue,
    // marginLeft: -5
  },
  cancelButton: {
    fontFamily: fonts.regular,
    color: colors.light_blue,
    fontSize: 16,
  },
  cancelIcon: {
    fontFamily: fonts.regular,
    color: colors.light_blue,
    fontSize: 30,
  },
  wrapSearch: {
    paddingLeft: 10
  },
  searchIcon: {
    fontSize: 25,
    color: colors.dark_blue,
  },
  addIcon: {
    fontSize: 25,
    color: colors.light_blue,
    borderLeftWidth: 1,
    borderLeftColor: colors.light_gray,
    paddingLeft: 21,
    marginLeft: 21
  }
});

export default HeaderSection;
