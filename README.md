# Protótipo App Stric

Este é um projeto de protótipo desenvolvido usando React Native, Expo e Typescript para a fintech Stric.

## Instalação

Use o git para clonar o repositório:
```bash
git clone git@bitbucket.org:rodrigoap23/stric.git
```
Em seguida use o [yarn](https://yarnpkg.com/) para instalar as dependências.

```bash
cd stric && yarn install
```

## Uso
Após as dependências instaladas, rode o comando
```bash
yarn start
```


## License
[MIT](https://choosealicense.com/licenses/mit/)