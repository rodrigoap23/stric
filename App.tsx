import { createDrawerNavigator } from "@react-navigation/drawer";
import AppLoading from "expo-app-loading";
import * as Font from "expo-font";
import React, { useState } from "react";
import Routes from "./src/routes";

const Drawer = createDrawerNavigator();

const fetchFont = () => {
  return Font.loadAsync({
    "Flexo-Regular": require("./src/assets/fonts/Flexo-Regular.ttf"),
    "Flexo-Medium": require("./src/assets/fonts/Flexo-Medium.ttf"),
    "Flexo-Bold": require("./src/assets/fonts/Flexo-Bold.ttf"),
  });
};

export default function App() {
  const [fontLoaded, setfontLoaded] = useState(false);
  if (!fontLoaded) {
    return (
      <AppLoading
        startAsync={fetchFont}
        onError={() => {
          console.log("Erro");
        }}
        onFinish={() => {
          setfontLoaded(true);
        }}
      />
    );
  }
  return <Routes />;
}
